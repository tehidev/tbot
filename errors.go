package tbot

import (
	"errors"
	"fmt"
)

var (
	errQueueNoUpdates = errors.New("no updates")
)

type ErrClient struct {
	Err error

	Code    int
	Method  string
	Details string
}

func (e *ErrClient) Unwrap() error { return e.Err }

func (e *ErrClient) Error() string {
	// variants:
	// {Method: method, Code: res.StatusCode}
	// {Method: method, Err: err}
	// {Method: method, Err: err, Details: "text"}
	// {Method: method, Err: err, Details: "text", Code: res.StatusCode}

	if e.Err == nil {
		return fmt.Sprintf("method '%s' code '%d'", e.Method, e.Code)
	}
	if e.Code == 0 {
		if e.Details == "" {
			return fmt.Sprintf("method '%s' error '%s'", e.Method, e.Err)
		}
		return fmt.Sprintf("method '%s' details '%s' error '%s'", e.Method, e.Details, e.Err)
	}
	return fmt.Sprintf("method '%s' code '%d' details '%s' error '%s'", e.Method, e.Code, e.Details, e.Err)
}

type ErrRuntime struct {
	Err    error
	Cursor *Cursor
	Update *Update
}

func (e *ErrRuntime) Unwrap() error { return e.Err }

func (e *ErrRuntime) Error() string {
	return e.Err.Error()
}
