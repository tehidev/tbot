package tbot

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"
)

type Context struct {
	context.Context
	*Cursor

	Update *Update
	Params ActionParams

	runtime *Runtime

	nextLine   bool
	nextBranch string
}

func (ctx *Context) NoUpdate() bool {
	return ctx.Update == nil
}

func (ctx *Context) HasUpdate() bool {
	return ctx.Update != nil
}

func (ctx *Context) Param(key string) string {
	return ctx.Params[key]
}

func (ctx *Context) ParamDuration(key string) (time.Duration, error) {
	return time.ParseDuration(ctx.Params[key])
}

func (ctx *Context) Next() error {
	ctx.nextLine = true
	return nil
}

func (ctx *Context) Jump(branchName string) error {
	if branchName == "" {
		return errors.New("juml to empty branch")
	}
	if !ctx.runtime.desc.HasBranch(branchName) {
		return fmt.Errorf("branch '%s' not found", branchName)
	}
	ctx.nextBranch = branchName
	return nil
}

func (ctx *Context) JumpParam(branchNameParam string) error {
	return ctx.Jump(ctx.Param(branchNameParam))
}

func (ctx *Context) Clear(key string) error {
	return ctx.runtime.storage.DelString(ctx.ChatID, key)
}

func (ctx *Context) GetString(key string) (string, error) {
	return ctx.runtime.storage.GetString(ctx.ChatID, key)
}

func (ctx *Context) PutString(key, val string) error {
	return ctx.runtime.storage.PutString(ctx.ChatID, key, val)
}

func (ctx *Context) GetInt64(key string) (int64, error) {
	s, err := ctx.GetString(key)
	if err != nil {
		return 0, err
	}
	return strconv.ParseInt(s, 10, 64)
}

func (ctx *Context) PutInt64(key string, val int64) error {
	return ctx.PutString(key, strconv.FormatInt(val, 10))
}

func (ctx *Context) GetInt(key string) (int, error) {
	s, err := ctx.GetString(key)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(s)
}

func (ctx *Context) PutInt(key string, val int) error {
	return ctx.PutString(key, strconv.Itoa(val))
}

func (ctx *Context) GetBool(key string) (bool, error) {
	s, err := ctx.GetString(key)
	if err != nil {
		return false, err
	}
	return strconv.ParseBool(s)
}

func (ctx *Context) PutBool(key string, val bool) error {
	return ctx.PutString(key, strconv.FormatBool(val))
}

func (ctx *Context) PutObject(key string, obj interface{}) error {
	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	return ctx.PutString(key, string(data))
}

func (ctx *Context) GetObject(key string, dst interface{}) error {
	val, err := ctx.GetString(key)
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(val), dst)
}
