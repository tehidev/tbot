package tbot

import (
	"gitlab.com/tehidev/go/tbot/codes"
)

type Cursor struct {
	ChatID int64 `json:"chatID"`

	Language codes.Language `json:"language"`

	BranchName string `json:"branchName"`
	BranchLine int    `json:"branchLine"`
	ActionName string `json:"actionName"`
}
