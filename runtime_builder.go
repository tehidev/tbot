package tbot

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"

	"gitlab.com/tehidev/go/tbot/codes"
)

type RuntimeBuilder struct {
	r *Runtime

	descData []byte
	descFile string
}

func NewRuntime() *RuntimeBuilder {
	return &RuntimeBuilder{r: &Runtime{
		context:         context.Background(),
		storage:         newRuntimeMemoryStorage(),
		defaultLanguage: codes.English,
		printError: func(err error) {
			log.Println(err)
		},
		actions: make(map[string]Action),

		mx: newValueMutex(),
	}}
}

func (b *RuntimeBuilder) WithContext(ctx context.Context) *RuntimeBuilder {
	b.r.context = ctx
	return b
}

func (b *RuntimeBuilder) WithStorage(storage RuntimeStorage) *RuntimeBuilder {
	b.r.storage = storage
	return b
}

func (b *RuntimeBuilder) WithDefaultLanguage(lang codes.Language) *RuntimeBuilder {
	b.r.defaultLanguage = lang
	return b
}

func (b *RuntimeBuilder) WithErrorHandler(h func(error)) *RuntimeBuilder {
	b.r.printError = h
	return b
}

func (b *RuntimeBuilder) WithDescriptionData(data []byte) *RuntimeBuilder {
	b.descData = data
	return b
}

func (b *RuntimeBuilder) WithDescriptionFile(path string) *RuntimeBuilder {
	b.descFile = path
	return b
}

func (b *RuntimeBuilder) WithAction(name string, action Action) *RuntimeBuilder {
	if name != "" && action != nil {
		b.r.actions[name] = action
	}
	return b
}

func (b *RuntimeBuilder) Build() (*Runtime, error) {
	if b.r.storage == nil {
		return nil, errors.New("runtime.Storage is no set")
	}
	if b.r.printError == nil {
		return nil, errors.New("runtime.ErrorHandler is no set")
	}

	if b.descFile != "" {
		data, err := os.ReadFile(b.descFile)
		if err != nil {
			return nil, err
		}
		b.descData = data
	}

	if b.r.defaultLanguage == "" {
		return nil, errors.New("runtime.DefaultLanguage is no set")
	}

	if b.descData != nil {
		desc, err := ParseDescription(b.descData, b.r.defaultLanguage)
		if err != nil {
			return nil, err
		}
		b.r.desc = desc
	}

	if b.r.desc == nil {
		return nil, errors.New("runtime.Description is no set")
	}

	for _, actionName := range b.r.desc.Actions {
		if _, ok := b.r.actions[actionName]; !ok {
			return nil, fmt.Errorf("runtime.Action '%s' not found", actionName)
		}
	}

	return b.r, nil
}
