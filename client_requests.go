package tbot

import (
	"bytes"
	"mime/multipart"
	"strconv"

	"gitlab.com/tehidev/go/tbot/codes"
)

// Use this method to send invoices. On success, the sent Message is returned.
type RequestSendInvoice struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Product name, 1-32 characters
	Title string `json:"title,omitempty"`
	// Product description, 1-255 characters
	Description string `json:"description,omitempty"`
	// Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
	Payload string `json:"payload,omitempty"`
	// Payments provider token, obtained via Botfather
	ProviderToken string `json:"provider_token,omitempty"`
	// Three-letter ISO 4217 currency code, see more on currencies
	Currency string `json:"currency,omitempty"`
	// Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
	Prices []*LabeledPrice `json:"prices,omitempty"`
	// Optional. The maximum accepted amount for tips in the smallest units of the currency (int64, not float/double).
	// For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145.
	// See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
	MaxTipAmount int64 `json:"max_tip_amount,omitempty"`
	// Optional. A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (int64, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
	SuggestedTipAmounts []int64 `json:"suggested_tip_amounts,omitempty"`
	// Optional. Unique deep-linking parameter. If left empty, forwarded copies of the sent message will have a Pay button,
	// allowing multiple users to pay directly from the forwarded message, using the same invoice.
	// If non-empty, forwarded copies of the sent message will have a URL button with a deep link to the bot (instead of a Pay button), with the value used as the start parameter[]int64
	StartParameter string `json:"start_parameter,omitempty"`
	// Optional. A JSON-serialized data about the invoice, which will be shared with the payment provider.
	// A detailed description of required fields should be provided by the payment provider.
	ProviderData string `json:"provider_data,omitempty"`
	// Optional. URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
	// People like it better when they see what they are paying for.ng
	PhotoURL string `json:"photo_url,omitempty"`
	// Optional. Photo size
	PhotoSize int64 `json:"photo_size,omitempty"`
	// Optional. Photo width
	PhotoWidth int `json:"photo_width,omitempty"`
	// Optional. Photo height
	PhotoHeight int `json:"photo_height,omitempty"`
	// Optional. Pass True, if you require the user's full name to complete the orderger
	NeedName bool `json:"need_name,omitempty"`
	// Optional. Pass True, if you require the user's phone number to complete the order
	NeedPhoneNumber bool `json:"need_phone_number,omitempty"`
	// Optional. Pass True, if you require the user's email address to complete the orderbool
	NeedEmail bool `json:"need_email,omitempty"`
	// Optional. Pass True, if you require the user's shipping address to complete the order
	NeedShippingAddress bool `json:"need_shipping_address,omitempty"`
	// Optional. Pass True, if user's phone number should be sent to provider
	SendPhoneNumberToProvider bool `json:"send_phone_number_to_provider,omitempty"`
	// Optional. Pass True, if user's email address should be sent to providerbool
	SendEmailToProvider bool `json:"send_email_to_provider,omitempty"`
	// Optional. Pass True, if the final price depends on the shipping methodoolean
	IsFlexible bool `json:"is_flexible,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and savingbool
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to create a link for an invoice. Returns the created invoice link as String on success
type RequestCreateInvoiceLink struct {
	// Product name, 1-32 characters
	Title string `json:"title"`
	// Product description, 1-255 characters
	Description string `json:"description"`
	// Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
	Payload string `json:"payload"`
	// Payment provider token, obtained via BotFather
	ProviderToken string `json:"provider_token"`
	// Three-letter ISO 4217 currency code, see more on currencies
	Currency string `json:"currency"`
	// Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
	Prices []LabeledPrice `json:"prices"`
	// Optional. The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example,
	// The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double).
	// For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json,
	// it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
	MaxTipAmount int `json:"max_tip_amount"`
	// Optional. A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (integer, not float/double).
	// At most 4 suggested tip amounts can be specified.
	// The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
	SuggestedTipAmounts []int `json:"suggested_tip_amounts"`
	// Optional. JSON-serialized data about the invoice, which will be shared with the payment provider.
	// A detailed description of required fields should be provided by the payment provider.
	ProviderData string `json:"provider_data"`
	// Optional. URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
	PhotoURL string `json:"photo_url"`
	// Optional. Photo size in bytes
	PhotoSize int `json:"photo_size"`
	// Optional. Photo width
	PhotoWidth int `json:"photo_width"`
	// Optional. Photo height
	PhotoHeight int `json:"photo_height"`
	// Optional. Pass True if you require the user's full name to complete the order
	NeedName bool `json:"need_name"`
	// Optional. Pass True if you require the user's phone number to complete the order
	NeedPhoneNumber bool `json:"need_phone_number"`
	// Optional. Pass True if you require the user's email address to complete the order
	NeedEmail bool `json:"need_email"`
	// Optional. Pass True if you require the user's shipping address to complete the order
	NeedShippingAddress bool `json:"need_shipping_address"`
	// Optional. Pass True if the user's phone number should be sent to the provider
	SendPhoneNumberToProvider bool `json:"send_phone_number_to_provider"`
	// Optional. Pass True if the user's email address should be sent to the provider
	SendEmailToProvider bool `json:"send_email_to_provider"`
	// Optional. Pass True if the final price depends on the shipping method
	IsFlexible bool `json:"is_flexible"`
}

// If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will
// send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success, True is returned.
type RequestAnswerShippingQuery struct {
	//	Unique identifier for the query to be answered
	ShippingQueryID string `json:"shipping_query_id"`
	// Specify True if delivery to the specified address is possible and False if there are any problems
	// (for example, if delivery to the specified address is not possible)
	OK bool `json:"ok"`
	// Optional. Required if ok is True. A JSON-serialized array of available shipping options.
	ShippingOptions []*ShippingOption `json:"shipping_options"`
	// Optional. Required if ok is False. Error message in human readable form that explains why it is impossible to complete the order
	// (e.g. "Sorry, delivery to your desired address is unavailable'). Telegram will display this message to the user.
	ErrorMessage string `json:"error_message"`
}

// Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of
// an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries.
// On success, True is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
type RequestAnswerPreCheckoutQuery struct {
	//	Unique identifier for the query to be answered
	PreCheckoutQueryID string `json:"pre_checkout_query_id"`
	// Specify True if everything is alright (goods are available, etc.) and the bot is ready to proceed with the order. Use False if there are any problems.
	OK bool `json:"ok"`
	// Optional. Required if ok is False. Error message in human readable form that explains the reason
	// for failure to proceed with the checkout (e.g. "Sorry, somebody just bought the last of our amazing
	// black T-shirts while you were busy filling out your payment details. Please choose a different color or garment!").
	// Telegram will display this message to the user.
	ErrorMessage string `json:"error_message"`
}

// Informs a user that some of the Telegram Passport elements they provided contains errors.
// The user will not be able to re-submit their Passport to you until the errors are fixed
// (the contents of the field for which you returned the error must change).
// Returns True on success.
//
// Use this if the data submitted by the user doesn't satisfy the standards your service requires for any reason.
// For example, if a birthday date seems invalid, a submitted document is blurry, a scan shows evidence of tampering, etc.
// Supply some details in the error message to make sure the user knows how to correct the issues
type RequestSetPassportDataErrors struct {
	// User identifier
	UserID int64 `json:"user_id"`
	// A JSON-serialized array describing the errors
	Errors []PassportElementError `json:"errors"`
}

// Use this method to send text messages. On success, the sent Message is returned
type RequestSendMessage struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Text of the message to be sent, 1-4096 characters after entities parsing
	Text string `json:"text,omitempty"`
	// Optional. Mode for parsing entities in the message text. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
	Entities []*MessageEntity `json:"entities,omitempty"`
	// Optional. Disables link previews for links in this message
	DisableWebPagePreview bool `json:"disable_web_page_preview,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and savingbool
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

// Use this method to forward messages of any kind. Service messages can't be forwarded. On success, the sent Message is returned.
type RequestForwardMessage struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Unique identifier for the chat where the original message was sent (or channel username in the format @channelusername)
	FromChatID int64 `json:"from_chat_id,omitempty"`
	// Message identifier in the chat specified in from_chat_id
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the forwarded message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
}

// Use this method to send photos. On success, the sent Message is returned.
type RequestSendPhoto struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Photo to send. Pass a file_id as string to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a string for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. The photo must be at most 10 MB in size. The photo's width and height must not exceed 10000 in total. Width and height ratio must be at most 20. More info on Sending Files »
	Photo SendFile `json:"photo,omitempty"`
	// Optional. Photo caption (may also be used when resending photos by file_id), 0-1024 characters after entities parsing
	Caption string `json:"caption,omitempty"`
	// Optional. Mode for parsing entities in the photo caption. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

func (r *RequestSendPhoto) IsMultipart() bool {
	return r.Photo != nil && r.Photo.IsMultipart()
}

func (r *RequestSendPhoto) MultipartForm() (data []byte, ctype string, err error) {
	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)

	_ = writer.WriteField("chat_id", strconv.FormatInt(r.ChatID, 10))

	if r.Photo != nil {
		_ = r.Photo.WriteTo("photo", writer)
	}
	if r.Caption != "" {
		_ = writer.WriteField("caption", r.Caption)
	}

	err = writer.Close()
	ctype = writer.FormDataContentType()
	data = buf.Bytes()
	return
}

// Use this method to send audio files, if you want Telegram clients to display them in the music player.
// Your audio must be in the .MP3 or .M4A format. On success, the sent Message is returned.
// Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.
// For sending voice messages, use the sendVoice method instead.
type RequestSendAudio struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Audio file to send. Pass a file_id as string to send an audio file that exists on the Telegram servers (recommended), pass an HTTP URL as a string for Telegram to get an audio file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
	Audio SendFile `json:"audio,omitempty"`
	// Optional. Audio caption, 0-1024 characters after entities parsing
	Caption string `json:"caption,omitempty"`
	// Optional. Mode for parsing entities in the audio caption. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities,omitempty"`
	// Optional. Duration of the audio in seconds
	Duration int64 `json:"duration,omitempty"`
	// Optional. Performer
	Performer string `json:"performer,omitempty"`
	// Optional. Track name
	Title string `json:"title,omitempty"`
	// Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
	Thumb SendFile `json:"thumb,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

func (r *RequestSendAudio) IsMultipart() bool {
	return r.Audio != nil && r.Audio.IsMultipart() || r.Thumb != nil && r.Thumb.IsMultipart()
}

func (r *RequestSendAudio) MultipartForm() (data []byte, ctype string, err error) {
	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)

	_ = writer.WriteField("chat_id", strconv.FormatInt(r.ChatID, 10))
	if r.Caption != "" {
		_ = writer.WriteField("caption", r.Caption)
	}
	if r.Audio != nil {
		_ = r.Audio.WriteTo("audio", writer)
	}
	if r.Thumb != nil {
		_ = r.Thumb.WriteTo("thumb", writer)
	}

	err = writer.Close()
	ctype = writer.FormDataContentType()
	data = buf.Bytes()
	return
}

// Use this method to send general files. On success, the sent Message is returned.
// Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
type RequestSendDocument struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// File to send. Pass a file_id as string to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a string for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
	Document SendFile `json:"document,omitempty"`
	// Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
	Thumb SendFile `json:"thumb,omitempty"`
	// Optional. Document caption (may also be used when resending documents by file_id), 0-1024 characters after entities parsing
	Caption string `json:"caption,omitempty"`
	// Optional. Mode for parsing entities in the document caption. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities,omitempty"`
	// Optional. Disables automatic server-side content type detection for files uploaded using multipart/form-data
	DisableContentTypeDetection bool `json:"disable_content_type_detection,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

func (r *RequestSendDocument) IsMultipart() bool {
	return r.Document != nil && r.Document.IsMultipart() || r.Thumb != nil && r.Thumb.IsMultipart()
}

func (r *RequestSendDocument) MultipartForm() (data []byte, ctype string, err error) {
	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)

	_ = writer.WriteField("chat_id", strconv.FormatInt(r.ChatID, 10))
	if r.Caption != "" {
		_ = writer.WriteField("caption", r.Caption)
	}
	if r.Document != nil {
		_ = r.Document.WriteTo("document", writer)
	}
	if r.Thumb != nil {
		_ = r.Thumb.WriteTo("thumb", writer)
	}

	err = writer.Close()
	ctype = writer.FormDataContentType()
	data = buf.Bytes()
	return
}

// Use this method to send video files, Telegram clients support mp4 videos (other formats may be sent as Document).
// On success, the sent Message is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
type RequestSendVideo struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Video to send. Pass a file_id as string to send a video that exists on the Telegram servers (recommended), pass an HTTP URL as a string for Telegram to get a video from the Internet, or upload a new video using multipart/form-data. More info on Sending Files »
	Video SendFile `json:"video,omitempty"`
	// Optional. Duration of sent video in seconds
	Duration int64 `json:"duration,omitempty"`
	// Optional. Video width
	Width int `json:"width,omitempty"`
	// Optional. Video height
	Height int `json:"height,omitempty"`
	// Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
	Thumb SendFile `json:"thumb,omitempty"`
	// Optional. Video caption (may also be used when resending videos by file_id), 0-1024 characters after entities parsing
	Caption string `json:"caption,omitempty"`
	// Optional. Mode for parsing entities in the video caption. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities,omitempty"`
	// Optional. Pass True, if the uploaded video is suitable for streaming
	SupportsStreaming bool `json:"supports_streaming,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

func (r *RequestSendVideo) IsMultipart() bool {
	return r.Video != nil && r.Video.IsMultipart() || r.Thumb != nil && r.Thumb.IsMultipart()
}

func (r *RequestSendVideo) MultipartForm() (data []byte, ctype string, err error) {
	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)

	_ = writer.WriteField("chat_id", strconv.FormatInt(r.ChatID, 10))
	if r.Caption != "" {
		_ = writer.WriteField("caption", r.Caption)
	}
	if r.Video != nil {
		_ = r.Video.WriteTo("video", writer)
	}
	if r.Thumb != nil {
		_ = r.Thumb.WriteTo("thumb", writer)
	}

	err = writer.Close()
	ctype = writer.FormDataContentType()
	data = buf.Bytes()
	return
}

// Use this method to send a group of photos, videos, documents or audios as an album.
// Documents and audio files can be only grouped in an album with messages of the same type.
// On success, an array of Messages that were sent is returned.
type RequestSendMediaGroup struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// A JSON-serialized array describing messages to be sent, must include 2-10 items
	// array of InputMediaAudio, InputMediaDocument, InputMediaPhoto and InputMediaVideo
	Media []InputMedia `json:"media,omitempty"`
	// Optional. Sends messages silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent messages from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the messages are a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
}

// Use this method to send point on the map. On success, the sent Message is returned.
type RequestSendLocation struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Latitude of the location
	Latitude float64 `json:"latitude,omitempty"`
	// Longitude of the location
	Longitude float64 `json:"longitude,omitempty"`
	// Optional. The radius of uncertainty for the location, measured in meters; 0-1500
	HorizontalAccuracy float64 `json:"horizontal_accuracy,omitempty"`
	// Optional. Period in seconds for which the location will be updated (see Live Locations, should be between 60 and 86400.
	LivePeriod int64 `json:"live_period,omitempty"`
	// Optional. For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
	Heading int64 `json:"heading,omitempty"`
	// Optional. For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
	ProximityAlertRadius int64 `json:"proximity_alert_radius,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

// Use this method to edit live location messages. A location can be edited until its live_period expires
// or editing is explicitlydisabled by a call to stopMessageLiveLocation.
// On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestEditMessageLiveLocation struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message to edit
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// Latitude of new location
	Latitude float64 `json:"latitude,omitempty"`
	// Longitude of new location
	Longitude float64 `json:"longitude,omitempty"`
	// Optional. The radius of uncertainty for the location, measured in meters; 0-1500
	HorizontalAccuracy float64 `json:"horizontal_accuracy,omitempty"`
	// Optional. Direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
	Heading int64 `json:"heading,omitempty"`
	// Optional. Maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
	ProximityAlertRadius int64 `json:"proximity_alert_radius,omitempty"`
	// Optional. A JSON-serialized object for a new inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to stop updating a live location message before live_period expires.
// On success, if the message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestStopMessageLiveLocation struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message with live location to stop
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// Optional. A JSON-serialized object for a new inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to send information about a venue. On success, the sent Message is returned.
type RequestSendVenue struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Latitude of the venue
	Latitude float64 `json:"latitude,omitempty"`
	// Longitude of the venue
	Longitude float64 `json:"longitude,omitempty"`
	// Name of the venue
	Title string `json:"title,omitempty"`
	// Address of the venue
	Address string `json:"address,omitempty"`
	// Optional. Foursquare identifier of the venue
	FoursquareID string `json:"foursquare_id,omitempty"`
	// Optional. Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
	FoursquareType string `json:"foursquare_type,omitempty"`
	// Optional. Google Places identifier of the venue
	GooglePlaceID string `json:"google_place_id,omitempty"`
	// Optional. Google Places type of the venue. (See supported types.)
	GooglePlaceType string `json:"google_place_type,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard,
	// instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

// Use this method when you need to tell the user that something is happening on the bot's side.
// The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns True on success.
type RequestSendChatAction struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id"`
	// Type of action to broadcast. Choose one, depending on what the user is about to receive:
	// typing for text messages, upload_photo for photos, record_video or upload_video for videos, record_voice or upload_voice for voice notes,
	// upload_document for general files, choose_sticker for stickers, find_location for location data, record_video_note or upload_video_note for video notes.
	Action ChatAction `json:"action"`
}

// Use this method to get basic info about a file and prepare it for downloading.
// For the moment, bots can download files of up to 20MB in size. On success, a File object is returned.
// The file can then be downloaded via the link https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is taken from the response.
// It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile again.
type RequestGetFile struct {
	// File identifier to get info about
	FileID string `json:"file_id"`
}

// Use this method to add a message to the list of pinned messages in a chat.
// If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must
// have the 'can_pin_messages' administrator right in a supergroup or 'can_edit_messages' administrator right in a channel. Returns True on success.
type RequestPinChatMessage struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Identifier of a message to pin
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Pass True, if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels and private chats.
	DisableNotification bool `json:"disable_notification,omitempty"`
}

// Use this method to remove a message from the list of pinned messages in a chat.
// If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must
// have the 'can_pin_messages' administrator right in a supergroup or 'can_edit_messages' administrator right in a channel. Returns True on success.
type RequestUnpinChatMessage struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Identifier of a message to unpin. If not specified, the most recent pinned message (by sending date) will be unpinned.
	MessageID int64 `json:"message_id,omitempty"`
}

// Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must
// be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right in a supergroup
// or 'can_edit_messages' administrator right in a channel. Returns True on success.
type RequestUnpinAllChatMessages struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
}

// Use this method for your bot to leave a group, supergroup or channel. Returns True on success.
type RequestLeaveChat struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
}

// Use this method to get up to date information about the chat (current name of the user for one-on-one conversations,
// current username of a user, group or channel, etc.). Returns a Chat object on success.
type RequestGetChat struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
}

// Use this method to edit text and game messages.
// On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestEditMessageText struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message to edit
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// New text of the message, 1-4096 characters after entities parsing
	Text string `json:"text,omitempty"`
	// Optional. Mode for parsing entities in the message text. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
	Entities []*MessageEntity `json:"entities,omitempty"`
	// Optional. Disables link previews for links in this message
	DisableWebPagePreview bool `json:"disable_web_page_preview,omitempty"`
	// Optional. A JSON-serialized object for an inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to edit captions of messages.
// On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestEditMessageCaption struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message to edit
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// Optional. New caption of the message, 0-1024 characters after entities parsing
	Caption string `json:"caption,omitempty"`
	// Optional. Mode for parsing entities in the message caption. See formatting options for more details.
	ParseMode string `json:"parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities,omitempty"`
	// Optional. A JSON-serialized object for an inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to edit animation, audio, document, photo, or video messages.
// If a message is part of a message album, then it can be edited only to an audio for audio albums,
// only to a document for document albums and to a photo or a video otherwise.
// When an inline message is edited, a new file can't be uploaded; use a previously uploaded file via its file_id or specify a URL.
// On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestEditMessageMedia struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message to edit
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// A JSON-serialized object for a new media content of the message
	Media InputMedia `json:"media"`
	// Optional. A JSON-serialized object for an inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to edit only the reply markup of messages.
// On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type RequestEditMessageReplyMarkup struct {
	// Optional. Required if inline_message_id is not specified. Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. Required if inline_message_id is not specified. Identifier of the message to edit
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. Required if chat_id and message_id are not specified. Identifier of the inline message
	InlineMessageID string `json:"inline_message_id,omitempty"`
	// Optional. A JSON-serialized object for an inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to stop a poll which was sent by the bot. On success, the stopped Poll is returned.
type RequestStopPoll struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Identifier of the original message with the poll
	MessageID int64 `json:"message_id,omitempty"`
	// Optional. A JSON-serialized object for a new message inline keyboard.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}

// Use this method to delete a message, including service messages, with the following limitations:
// - A message can only be deleted if it was sent less than 48 hours ago.
// - A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.
// - Bots can delete outgoing messages in private chats, groups, and supergroups.
// - Bots can delete incoming messages in private chats.
// - Bots granted can_post_messages permissions can delete outgoing messages in channels.
// - If the bot is an administrator of a group, it can delete any message there.
// - If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.
// Returns True on success.
type RequestDeleteMessage struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Identifier of the message to delete
	MessageID int64 `json:"message_id,omitempty"`
}

// Use this method to send a native poll. On success, the sent Message is returned.
type RequestSendPoll struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id,omitempty"`
	// Poll question, 1-300 characters
	Question string `json:"question,omitempty"`
	// A JSON-serialized list of answer options, 2-10 strings 1-100 characters each
	Options []string `json:"options,omitempty"`
	// Optional. True, if the poll needs to be anonymous, defaults to True
	IsAnonymous bool `json:"is_anonymous,omitempty"`
	// Optional. Poll type, “quiz” or “regular”, defaults to “regular”
	Type string `json:"type,omitempty"`
	// Optional. True, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to False
	AllowsMultipleAnswers bool `json:"allows_multiple_answers,omitempty"`
	// Optional. 0-based identifier of the correct answer option, required for polls in quiz mode
	CorrectOptionID int64 `json:"correct_option_id,omitempty"`
	// Optional. Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds after entities parsing
	Explanation string `json:"explanation,omitempty"`
	// Optional. Mode for parsing entities in the explanation. See formatting options for more details.
	ExplanationParseMode string `json:"explanation_parse_mode,omitempty"`
	// Optional. A JSON-serialized list of special entities that appear in the poll explanation, which can be specified instead of parse_mode
	ExplanationEntities []*MessageEntity `json:"explanation_entities,omitempty"`
	// Optional. Amount of time in seconds the poll will be active after creation, 5-600. Can't be used together with close_date.
	OpenPeriod int64 `json:"open_period,omitempty"`
	// Optional. Point in time (Unix timestamp) when the poll will be automatically closed. Must be at least 5 and no more than 600 seconds in the future. Can't be used together with open_period.
	CloseDate Unix `json:"close_date,omitempty"`
	// Optional. Pass True, if the poll needs to be immediately closed. This can be useful for poll preview.
	IsClosed bool `json:"is_closed,omitempty"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True, if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup,omitempty"`
}

// Use this method to receive incoming updates using long polling (wiki). An Array of Update objects is returned.
type RequestGetUpdates struct {
	// Optional. Identifier of the first update to be returned.
	// Must be greater by one than the highest among the identifiers of previously received updates.
	// By default, updates starting with the earliest unconfirmed update are returned.
	// An update is considered confirmed as soon as getUpdates is called with an offset higher than its update_id.
	// The negative offset can be specified to retrieve updates starting from -offset update from the end of the updates queue.
	// All previous updates will forgotten.
	Offset int64 `json:"offset,omitempty"`
	// Optional. Limits the number of updates to be retrieved. Values between 1-100 are accepted. Defaults to 100.
	Limit int64 `json:"limit,omitempty"`
	// Optional. Timeout in seconds for long polling. Defaults to 0, i.e. usual short polling.
	// Should be positive, short polling should be used for testing purposes only.
	Timeout int64 `json:"timeout,omitempty"`
	// Optional. A JSON-serialized list of the update types you want your bot to receive.
	// For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types.
	// See Update for a complete list of available update types. Specify an empty list to receive all update types except chat_member (default).
	// If not specified, the previous setting will be used.
	// Please note that this parameter doesn't affect updates created before the call to the getUpdates, so unwanted updates may be received for a short period of time.
	AllowedUpdates []string `json:"allowed_updates,omitempty"`
}

// Use this method to change the bot's menu button in a private chat, or the default menu button. Returns True on success.
type RequestSetChatMenuButton struct {
	// Optional. Unique identifier for the target private chat. If not specified, default bot's menu button will be changed
	ChatID int64 `json:"chat_id,omitempty"`
	// Optional. A JSON-serialized object for the bot's new menu button. Defaults to MenuButtonDefault
	MenuButton *MenuButton `json:"menu_button,omitempty"`
}

// Use this method to get the current value of the bot's menu button in a private chat, or the default menu button.
// Returns MenuButton on success.
type RequestGetChatMenuButton struct {
	// Optional. Unique identifier for the target private chat. If not specified, default bot's menu button will be returned
	ChatID int64 `json:"chat_id,omitempty"`
}

// Use this method to send answers to callback queries sent from inline keyboards.
// The answer will be displayed to the user as a notification at the top of the chat screen or as an alert. On success, True is returned.
// Alternatively, the user can be redirected to the specified Game URL.
// For this option to work, you must first create a game for your bot via @Botfather and accept the terms.
// Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
type RequestAnswerCallbackQuery struct {
	// Unique identifier for the query to be answered
	ID string `json:"callback_query_id"`
	// Optional	Text of the notification. If not specified, nothing will be shown to the user, 0-200 characters
	Text string `json:"text"`
	// Optional	If True, an alert will be shown by the client instead of a notification at the top of the chat screen. Defaults to false.
	ShowAlert bool `json:"show_alert"`
	// Optional	URL that will be opened by the user's client.
	// If you have created a Game and accepted the conditions via @Botfather, specify the URL that opens your game —
	// note that this will only work if the query comes from a callback_game button.
	// Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
	URL string `json:"url"`
	// Optional	The maximum amount of time in seconds that the result of the callback query may be cached client-side.
	// Telegram apps will support caching starting in version 3.14. Defaults to 0.
	CacheTime int64 `json:"cache_time"`
}

// Use this method to set the result of an interaction with a Web App and send
// a corresponding message on behalf of the user to the chat from which the query originated.
// On success, a SentWebAppMessage object is returned.
type RequestAnswerWebAppQuery struct {
	// Unique identifier for the query to be answered
	WebAppQueryID string `json:"web_app_query_id"`
	// A JSON-serialized object describing the message to be sent.
	// InlineQueryResult
	Result interface{} `json:"result"`
}

// Use this method to change the list of the bot's commands.
// See https://core.telegram.org/bots#commands for more details about bot commands. Returns True on success.
type RequestSetMyCommands struct {
	// A JSON-serialized list of bot commands to be set as the list of the bot's commands. At most 100 commands can be specified.
	Commands []*BotCommand `json:"commands"`
	// Optional. A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
	Scope *BotCommandScope `json:"scope,omitempty"`
	// Optional. A two-letter ISO 639-1 language code.
	// If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
	LanguageCode codes.Language `json:"language_code,omitempty"`
}

// Use this method to delete the list of the bot's commands for the given scope and user language.
// After deletion, higher level commands will be shown to affected users. Returns True on success.
type RequestDeleteMyCommands struct {
	// Optional. A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
	Scope *BotCommandScope `json:"scope,omitempty"`
	// Optional. A two-letter ISO 639-1 language code.
	// If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
	LanguageCode codes.Language `json:"language_code,omitempty"`
}

// Use this method to get the current list of the bot's commands for the given scope and user language.
// Returns Array of BotCommand on success. If commands aren't set, an empty list is returned.
type RequestGetMyCommands struct {
	// Optional. A JSON-serialized object, describing scope of users. Defaults to BotCommandScopeDefault.
	Scope *BotCommandScope `json:"scope,omitempty"`
	// Optional. A two-letter ISO 639-1 language code or an empty string
	LanguageCode codes.Language `json:"language_code,omitempty"`
}

// Use this method to get a list of administrators in a chat, which aren't bots.
// Returns an Array of ChatMember objects.
type RequestGetChatAdministrators struct {
	// Unique identifier for the target chat of the target supergroup or channel
	ChatID int64 `json:"chat_id"`
}

// Use this method to get the number of members in a chat.
// Returns Int on success
type RequestGetChatMemberCount struct {
	// Unique identifier for the target chat of the target supergroup or channel
	ChatID int64 `json:"chat_id"`
}

// Use this method to get information about a member of a chat.
// Returns a ChatMember object on success
type RequestGetChatMember struct {
	// Unique identifier for the target chat of the target supergroup or channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
}

type RequestGetUserProfilePhotos struct {
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// Optional. Sequential number of the first photo to be returned. By default, all photos are returned.
	Offset int `json:"offset"`
	// Optional. Limits the number of photos to be retrieved. Values between 1-100 are accepted. Defaults to 100.
	Limit int `json:"limit"`
}

// Use this method to copy messages of any kind.
// Service messages and invoice messages can't be copied.
// A quiz poll can be copied only if the value of the field correct_option_id is known to the bot.
// The method is analogous to the method forwardMessage, but the copied message doesn't have a link to the original message.
// Returns the MessageId of the sent message on success.
type RequestCopyMessage struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier for the chat where the original message was sent (or channel username
	FromChatID int64 `json:"from_chat_id"`
	// Message identifier in the chat specified in from_chat_id
	MessageID int64 `json:"message_id"`
	// Optional. New caption for media, 0-1024 characters after entities parsing. If not specified, the original caption is kept
	Caption string `json:"caption"`
	// Optional. Mode for parsing entities in the new caption. See formatting options for more details.
	ParseMode string `json:"parse_mode"`
	// Optional. A JSON-serialized list of special entities that appear in the new caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

// Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound).
// On success, the sent Message is returned.
// Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future
type RequestSendAnimation struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Animation to send.
	// Pass a file_id as String to send an animation that exists on the Telegram servers (recommended),
	// pass an HTTP URL as a String for Telegram to get an animation from the Internet, or upload a new animation using multipart/form-data
	Animation InputMedia `json:"animation"`
	// Optional. Duration of sent animation in seconds
	Duration int `json:"duration"`
	// Optional. Animation width
	Width int `json:"width"`
	// Optional. Animation height
	Height int `json:"height"`
	// Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side.
	// The thumbnail should be in JPEG format and less than 200 kB in size.
	// A thumbnail's width and height should not exceed 320.
	// Ignored if the file is not uploaded using multipart/form-data.
	// Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>”
	// if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
	Thumb InputMedia `json:"thumb"`
	// Optional. Animation caption (may also be used when resending animation by file_id), 0-1024 characters after entities parsing
	Caption string `json:"caption"`
	// Optional. Mode for parsing entities in the animation caption. See formatting options for more details.
	ParseMode string `json:"parse_mode"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

// Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message.
// For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or Document).
// On success, the sent Message is returned.
// Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future
type RequestSendVoice struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Audio file to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended),
	// pass an HTTP URL as a String for Telegram to get a file from the Internet,
	// or upload a new one using multipart/form-data
	Voice InputMedia `json:"voice"`
	// Optional. Voice caption (may also be used when resending voice by file_id), 0-1024 characters after entities parsing
	Caption string `json:"caption"`
	// Optional. Mode for parsing entities in the voice caption. See formatting options for more details.
	ParseMode string `json:"parse_mode"`
	// Optional. A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode
	CaptionEntities []*MessageEntity `json:"caption_entities"`
	// Optional. Duration of sent voice in seconds
	Duration int `json:"duration"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

// As of v.4.0, Telegram clients support rounded square MPEG4 videos of up to 1 minute long.
// Use this method to send video messages. On success, the sent Message is returned.
type RequestSendVideoNote struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Video note to send.
	// Pass a file_id as String to send a video note that exists on the Telegram servers (recommended) or upload a new video using multipart/form-data.
	// Sending video notes by a URL is currently unsupported
	VideoNote InputMedia `json:"video_note"`
	// Optional. Duration of sent video in seconds
	Duration int `json:"duration"`
	// Optional. Video width and height, i.e. diameter of the video message
	Length int `json:"length"`
	// Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side.
	// The thumbnail should be in JPEG format and less than 200 kB in size.
	// A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data.
	// Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>”
	// if the thumbnail was uploaded using multipart/form-data under <file_attach_name>
	Thumb InputMedia `json:"thumb"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

type RequestSendContact struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Contact's phone number
	PhoneNumber string `json:"phone_number"`
	// Contact's first name
	FirstName string `json:"first_name"`
	// Optional. Contact's last name
	LastName string `json:"last_name"`
	// Optional. Additional data about the contact in the form of a vCard, 0-2048 bytes
	Vcard string `json:"vcard"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

// Use this method to send an animated emoji that will display a random value. On success, the sent Message is returned.
type RequestSendDice struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Optional. Emoji on which the dice throw animation is based. Currently, must be one of “🎲”, “🎯”, “🏀”, “⚽”, “🎳”, or “🎰”.
	// Dice can have values 1-6 for “🎲”, “🎯” and “🎳”, values 1-5 for “🏀” and “⚽”, and values 1-64 for “🎰”. Defaults to “🎲”
	Emoji string `json:"emoji"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	AllowSendingWithoutReply bool `json:"allow_sending_without_reply"`
	// Optional. Additional interface options.
	// A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
	// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply
	ReplyMarkup ReplyMarkup `json:"reply_markup"`
}

// Use this method to ban a user in a group, a supergroup or a channel.
// In the case of supergroups and channels, the user will not be able to return to the chat on their own using invite links, etc., unless unbanned first.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns True on success.
type RequestBanChatMember struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// Optional. Date when the user will be unbanned, unix time.
	// If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever.
	// Applied for supergroups and channels only.
	UntilDate Unix `json:"until_date"`
	// Optional. Pass True to delete all messages from the chat for the user that is being removed.
	// If False, the user will be able to see messages in the group that were sent before the user was removed. Always True for supergroups and channels.
	RevokeMessages bool `json:"revoke_messages"`
}

// Use this method to unban a previously banned user in a supergroup or channel.
// The user will not return to the group or channel automatically, but will be able to join via link, etc.
// The bot must be an administrator for this to work.
// By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it.
// So if the user is a member of the chat they will also be removed from the chat.
// If you don't want this, use the parameter only_if_banned. Returns True on success
type RequestUnbanChatMember struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// Optional. Do nothing if the user is not banned
	OnlyIfBanned bool `json:"only_if_banned"`
}

// Use this method to restrict a user in a supergroup.
// The bot must be an administrator in the supergroup for this to work and must have the appropriate administrator rights.
// Pass True for all permissions to lift restrictions from a user. Returns True on success
type RequestRestrictChatMember struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// A JSON-serialized object for new user permissions
	Permissions *ChatPermissions `json:"permissions"`
	// Optional. Date when restrictions will be lifted for the user, unix time.
	// If user is restricted for more than 366 days or less than 30 seconds from the current time, they are considered to be restricted forever
	UntilDate Unix `json:"until_date"`
}

// Use this method to promote or demote a user in a supergroup or a channel.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Pass False for all boolean parameters to demote a user. Returns True on success
type RequestPromoteChatMember struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// Optional. Pass True if the administrator's presence in the chat is hidden
	IsAnonymous bool `json:"is_anonymous"`
	// Optional. Pass True if the administrator can access the chat event log, chat statistics, message statistics in channels,
	// see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
	CanManageChat bool `json:"can_manage_chat"`
	// Optional. Pass True if the administrator can create channel posts, channels only
	CanPostMessages bool `json:"can_post_messages"`
	// Optional. Pass True if the administrator can edit messages of other users and can pin messages, channels only
	CanEditMessages bool `json:"can_edit_messages"`
	// Optional. Pass True if the administrator can delete messages of other users
	CanDeleteMessages bool `json:"can_delete_messages"`
	// Optional. Pass True if the administrator can manage video chats
	CanManageVideoChats bool `json:"can_manage_video_chats"`
	// Optional. Pass True if the administrator can restrict, ban or unban chat members
	CanRestrictMembers bool `json:"can_restrict_members"`
	// Optional. Pass True if the administrator can add new administrators with a subset of their own privileges
	// or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by him
	CanPromoteMembers bool `json:"can_promote_members"`
	// Optional. Pass True if the administrator can change chat title, photo and other settingsers"`
	CanChangeInfo bool `json:"can_change_info"`
	// Optional. Pass True if the administrator can invite new users to the chat
	CanInviteUsers bool `json:"can_invite_users"`
	// Optional. Pass True if the administrator can pin messages, supergroups only
	CanPinMessages bool `json:"can_pin_messages"`
}

// Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns True on success.
type RequestSetChatAdministratorCustomTitle struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
	// New custom title for the administrator; 0-16 characters, emoji are not allowed
	CustomTitle string `json:"custom_title"`
}

// Use this method to ban a channel chat in a supergroup or a channel.
// Until the chat is unbanned, the owner of the banned chat won't be able to send messages on behalf of any of their channels.
// The bot must be an administrator in the supergroup or channel for this to work and must have the appropriate administrator rights. Returns True on success.
type RequestBanChatSenderChat struct {
	// Unique identifier for the target chat or username of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target sender chat
	SenderChatID int64 `json:"sender_chat_id"`
}

// Use this method to unban a previously banned channel chat in a supergroup or channel.
// The bot must be an administrator for this to work and must have the appropriate administrator rights. Returns True on success
type RequestUnbanChatSenderChat struct {
	// Unique identifier for the target chat or username of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target sender chat
	SenderChatID int64 `json:"sender_chat_id"`
}

// Use this method to set default chat permissions for all members.
// The bot must be an administrator in the group or a supergroup for this to work and must have the can_restrict_members administrator rights.
// Returns True on success
type RequestSetChatPermissions struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// A JSON-serialized object for new default chat permissions
	Permissions *ChatPermissions `json:"permissions"`
}

// Use this method to generate a new primary invite link for a chat; any previously generated primary link is revoked.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Returns the new invite link as String on success
type RequestExportChatInviteLink struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
}

// Use this method to create an additional invite link for a chat.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// The link can be revoked using the method revokeChatInviteLink. Returns the new invite link as ChatInviteLink object
type RequestCreateChatInviteLink struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Optional. Invite link name; 0-32 characters
	Name string `json:"name"`
	// Optional. Point in time (Unix timestamp) when the link will expire
	ExpireDate Unix `json:"expire_date"`
	// Optional. The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
	MemberLimit int `json:"member_limit"`
	// Optional. True, if users joining the chat via the link need to be approved by chat administrators. If True, member_limit can't be specified
	CreatesJoinRequest bool `json:"creates_join_request"`
}

// Use this method to edit a non-primary invite link created by the bot.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Returns the edited invite link as a ChatInviteLink object.
type RequestEditChatInviteLink struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// The invite link to edit
	InviteLink string `json:"invite_link"`
	// Optional. Invite link name; 0-32 characters
	Name string `json:"name"`
	// Optional. Point in time (Unix timestamp) when the link will expire
	ExpireDate Unix `json:"expire_date"`
	// Optional. The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
	MemberLimit int `json:"member_limit"`
	// Optional. True, if users joining the chat via the link need to be approved by chat administrators.
	// If True, member_limit can't be specified
	CreatesJoinRequest bool `json:"creates_join_request"`
}

// Use this method to revoke an invite link created by the bot.
// If the primary link is revoked, a new link is automatically generated.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Returns the revoked invite link as ChatInviteLink object
type RequestRevokeChatInviteLink struct {
	// Unique identifier of the target chat or username of the target channel
	ChatID int64 `json:"chat_id"`
	// The invite link to revoke
	InviteLink string `json:"invite_link"`
}

// Use this method to approve a chat join request.
// The bot must be an administrator in the chat for this to work and must have the can_invite_users administrator right.
// Returns True on success
type RequestApproveChatJoinRequest struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
}

// Use this method to decline a chat join request.
// The bot must be an administrator in the chat for this to work and must have the can_invite_users administrator right.
// Returns True on success
type RequestDeclineChatJoinRequest struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Unique identifier of the target user
	UserID int64 `json:"user_id"`
}

// Use this method to set a new profile photo for the chat. Photos can't be changed for private chats.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Returns True on success.
type RequestSetChatPhoto struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// New chat photo, uploaded using multipart/form-data
	Photo InputMedia `json:"photo"`
}

// Use this method to delete a chat photo. Photos can't be changed for private chats.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns True on success
type RequestDeleteChatPhoto struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
}

// Use this method to change the title of a chat. Titles can't be changed for private chats.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns True on success
type RequestSetChatTitle struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// New chat title, 1-255 characters
	Title string `json:"title"`
}

// Use this method to change the description of a group, a supergroup or a channel.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns True on success
type RequestSetChatDescription struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Optional. New chat description, 0-255 characters
	Description string `json:"description"`
}

// Use this method to set a new group sticker set for a supergroup.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success
type RequestSetChatStickerSet struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
	// Name of the sticker set to be set as the group sticker set
	StickerSetName string `json:"sticker_set_name"`
}

// Use this method to delete a group sticker set from a supergroup.
// The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
// Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success
type RequestDeleteChatStickerSet struct {
	// Unique identifier for the target chat of the target channel
	ChatID int64 `json:"chat_id"`
}

// Use this method to change the default administrator rights requested by the bot when it's added as an administrator to groups or channels.
// These rights will be suggested to users, but they are are free to modify the list before adding the bot. Returns True on success
type RequestSetMyDefaultAdministratorRights struct {
	// Optional. A JSON-serialized object describing new default administrator rights.
	// If not specified, the default administrator rights will be cleared.
	Rights *ChatAdministratorRights `json:"rights"`
	// Optional. Pass True to change the default administrator rights of the bot in channels.
	// Otherwise, the default administrator rights of the bot for groups and supergroups will be changed
	ForChannels bool `json:"for_channels"`
}

// Use this method to get the current default administrator rights of the bot. Returns ChatAdministratorRights on success.
type RequestGetMyDefaultAdministratorRights struct {
	// Optional. Pass True to get default administrator rights of the bot in channels.
	// Otherwise, default administrator rights of the bot for groups and supergroups will be returned
	ForChannels bool `json:"for_channels"`
}

// Use this method to send a game. On success, the sent Message is returned.
type RequestSendGame struct {
	// Unique identifier for the target chat
	ChatID int64 `json:"chat_id"`
	// Short name of the game, serves as the unique identifier for the game. Set up your games via @BotFather.
	GameShortName string `json:"game_short_name"`
	// Optional. Sends the message silently. Users will receive a notification with no sound.
	DisableNotification bool `json:"disable_notification,omitempty"`
	// Optional. Protects the contents of the sent message from forwarding and saving
	ProtectContent bool `json:"protect_content,omitempty"`
	// Optional. If the message is a reply, ID of the original message
	ReplyToMessageID int64 `json:"reply_to_message_id,omitempty"`
	// Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	Allow_sending_without_reply bool `json:"allow_sending_without_reply,omitempty"`
	// Optional. A JSON-serialized object for an inline keyboard. If empty, one 'Play game_title' button will be shown. If not empty, the first button must launch the game.
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup,omitempty"`
}
