package tbot

import (
	"errors"
	"fmt"
	"strings"

	"gopkg.in/yaml.v3"

	"gitlab.com/tehidev/go/tbot/codes"
)

const (
	StartBranch   = "start"
	DefaultBranch = "default"
)

type Description struct {
	FileBranches map[string][]interface{}             `yaml:"branches"`
	FileStrings  map[string]map[codes.Language]string `yaml:"strings"`

	Branches map[string][]*BranchLine `yaml:"-"`

	Actions   []string         `yaml:"-"`
	Languages []codes.Language `yaml:"-"`

	DefaultLanguage codes.Language
}

func (d *Description) HasBranch(branchName string) bool {
	_, ok := d.Branches[branchName]
	return ok
}

func (d *Description) HasAction(actionName string) bool {
	for _, name := range d.Actions {
		if name == actionName {
			return true
		}
	}
	return false
}

func (d *Description) HasLanguage(code codes.Language) bool {
	for _, lang := range d.Languages {
		if lang == code {
			return true
		}
	}
	return false
}

type BranchLine struct {
	IsBranch     bool
	BranchName   string
	ActionName   string
	ActionParams map[codes.Language]ActionParams
}

func ParseDescription(data []byte, defaultLanguage codes.Language) (*Description, error) {
	var desc Description

	if err := yaml.Unmarshal(data, &desc); err != nil {
		return nil, err
	}

	if err := desc.checkExistingBranch(StartBranch); err != nil {
		return nil, err
	}
	if err := desc.checkExistingBranch(DefaultBranch); err != nil {
		return nil, err
	}

	desc.Branches = make(map[string][]*BranchLine)
	desc.Actions = []string{}
	desc.DefaultLanguage = defaultLanguage

	if err := desc.findLanguages(); err != nil {
		return nil, err
	}

	for branchName, rawLines := range desc.FileBranches {
		branchLines, err := desc.parseRawBranchLines(rawLines)
		if err != nil {
			return nil, fmt.Errorf("branch '%s': %s", branchName, err)
		}
		desc.Branches[branchName] = branchLines
	}

	if err := desc.checkBranchesLinks(); err != nil {
		return nil, err
	}

	return &desc, nil
}

func (d *Description) checkExistingBranch(name string) error {
	if lines, ok := d.FileBranches[name]; ok {
		if len(lines) == 0 {
			return fmt.Errorf("empty '%s' branch", name)
		}
		return nil
	}
	return fmt.Errorf("need '%s' branch", name)
}

func (d *Description) findLanguages() error {
	languages := map[codes.Language]struct{}{}
	for _, langValue := range d.FileStrings {
		for lang := range langValue {
			languages[lang] = struct{}{}
		}
	}

	if len(languages) == 0 {
		languages[d.DefaultLanguage] = struct{}{}
	} else if _, ok := languages[d.DefaultLanguage]; !ok {
		return fmt.Errorf("DefaultLanguage '%s' not found in description", d.DefaultLanguage)
	}

	d.Languages = make([]codes.Language, 0, len(languages))
	for lang := range languages {
		d.Languages = append(d.Languages, lang)
	}
	return nil
}

func (d *Description) parseRawBranchLines(rawLines []interface{}) ([]*BranchLine, error) {
	lines := make([]*BranchLine, len(rawLines))
	for i, rawLine := range rawLines {
		line, err := d.parseRawBranchLine(rawLine)
		if err != nil {
			return nil, fmt.Errorf("line %d: %s", i, err)
		}
		lines[i] = line
	}
	return lines, nil
}

func (d *Description) parseRawBranchLine(rawLine interface{}) (*BranchLine, error) {
	if name, ok := rawLine.(string); ok {
		line := &BranchLine{
			IsBranch:   true,
			BranchName: name,
		}
		return line, nil
	}
	if action, ok := rawLine.(map[string]interface{}); ok {
		if len(action) > 1 {
			return nil, errors.New("invald action length")
		}
		for actionName, actionAttributes := range action {
			d.Actions = append(d.Actions, actionName)

			props, err := d.parseActionParams(actionAttributes)
			if err != nil {
				return nil, fmt.Errorf("action '%s': %s", actionName, err)
			}
			line := &BranchLine{
				IsBranch:     false,
				ActionName:   actionName,
				ActionParams: props,
			}
			return line, nil
		}
	}
	return nil, errors.New("invalid action")
}

func (d *Description) parseActionParams(actionAttributes interface{}) (map[codes.Language]ActionParams, error) {
	if actionAttributes == nil {
		langProps := make(map[codes.Language]ActionParams)
		for _, lang := range d.Languages {
			langProps[lang] = make(ActionParams)
		}
		return langProps, nil
	}

	if values, ok := actionAttributes.(map[string]interface{}); ok {
		langProps := make(map[codes.Language]ActionParams)

		for _, lang := range d.Languages {
			props := make(ActionParams)

			for key, iVal := range values {
				val := fmt.Sprint(iVal)

				if strings.HasPrefix(val, "$") {
					langValue, ok := d.FileStrings[val[1:]]
					if !ok {
						return nil, fmt.Errorf("key '%s': string '%s' not found in strings section", key, val[1:])
					}
					if value, ok := langValue[lang]; ok {
						props[key] = value
					} else if value, ok = langValue[d.DefaultLanguage]; ok {
						props[key] = value
					} else {
						for _, value := range langValue {
							props[key] = value
							break
						}
					}
				} else {
					props[key] = val
				}

				langProps[lang] = props
			}
		}

		return langProps, nil
	}

	return nil, errors.New("invalid props")
}

func (d *Description) checkBranchesLinks() error {
	for branchName, lines := range d.Branches {
		if len(lines) == 0 {
			return fmt.Errorf("branch '%s': doesn't have lines", branchName)
		}
		for i, line := range lines {
			if line.IsBranch {
				_, ok := d.Branches[line.BranchName]
				if !ok {
					return fmt.Errorf("branch '%s': line: '%d': branch '%s' not found", branchName, i, line.BranchName)
				}
			}
		}
	}
	return nil
}
