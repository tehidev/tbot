package tbot

import (
	"context"
	"errors"
	"net/http"
)

func NewClient() *ClientBuilder {
	ctx, ctxCancel := context.WithCancel(context.Background())
	return &ClientBuilder{c: &Client{
		ctx:       ctx,
		ctxCancel: ctxCancel,
		http:      http.DefaultClient,
		endPoint:  "https://api.telegram.org",
	}}
}

type ClientBuilder struct {
	c *Client
}

func (b *ClientBuilder) WithHTTPClient(client *http.Client) *ClientBuilder {
	b.c.http = client
	return b
}

func (b *ClientBuilder) WithEndPoint(endPoint string) *ClientBuilder {
	b.c.endPoint = endPoint
	return b
}

func (b *ClientBuilder) WithAccessToken(accessToken string) *ClientBuilder {
	b.c.accessToken = accessToken
	return b
}

func (b *ClientBuilder) Build() (*Client, error) {
	switch {
	case b.c.http == nil:
		return nil, errors.New("client.HttpClient is no set")
	case b.c.endPoint == "":
		return nil, errors.New("client.EndPoint is no set")
	case b.c.accessToken == "":
		return nil, errors.New("client.AccessToken is no set")
	}
	return b.c, nil
}
