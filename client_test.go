package tbot_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tehidev/go/tbot"
)

func TestClient(t *testing.T) {
	accessToken := os.Getenv("CLIENT_ACCESS_TOKEN")
	if accessToken == "" {
		t.SkipNow()
	}

	client, err := tbot.NewClient().
		WithAccessToken(accessToken).
		Build()
	assert.NoError(t, err)

	fileData, err := os.ReadFile("README.md")
	assert.NoError(t, err)

	var offset int64 = 0
	for {
		updates, err := client.GetUpdates(&tbot.RequestGetUpdates{
			Offset:  offset + 1,
			Limit:   100,
			Timeout: 10,
		})
		assert.NoError(t, err)

		for _, u := range updates {
			if u.ID > offset {
				offset = u.ID
			}

			if u.Message == nil {
				continue
			}

			println("offset", offset, "chat_id", u.Message.Chat.ID)

			switch u.Message.Text {
			case "board":
				_, err = client.SendMessage(&tbot.RequestSendMessage{
					ChatID: u.Message.Chat.ID,
					Text:   "Держи keyboard",
					ReplyMarkup: tbot.ReplyKeyboardMarkup{
						Keyboard: [][]*tbot.KeyboardButton{
							{
								{
									Text: "enter to google base",
									WebApp: &tbot.WebAppInfo{
										URL: "https://google.com",
									},
								},
							},
						},
					},
				})
				assert.NoError(t, err)
			case "menu":
				_, err = client.SendMessage(&tbot.RequestSendMessage{
					ChatID: u.Message.Chat.ID,
					Text:   "Держи кнопка-меню",
					ReplyMarkup: tbot.InlineKeyboardMarkup{
						InlineKeyboard: [][]*tbot.InlineKeyboardButton{
							{
								{
									Text: "enter to google app",
									WebApp: &tbot.WebAppInfo{
										URL: "https://www.google.am/search?q=kek&source=lnms&tbm=isch",
									},
								},
							},
						},
					},
				})
				assert.NoError(t, err)
			case "off":
				err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
					ChatID: u.Message.Chat.ID,
				})
				assert.NoError(t, err)
				_, err = client.SendMessage(&tbot.RequestSendMessage{
					ChatID: u.Message.Chat.ID,
					Text:   "off",
					ReplyMarkup: tbot.ReplyKeyboardRemove{
						RemoveKeyboard: true,
					},
				})
				assert.NoError(t, err)
			case "on":
				err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
					ChatID: u.Message.Chat.ID,
					MenuButton: &tbot.MenuButton{
						Type: tbot.MenuButtonWebApp,
						Text: "Enter to yandex app",
						WebApp: &tbot.WebAppInfo{
							URL: "https://ya.ru",
						},
					},
				})
				assert.NoError(t, err)
			case "photo":
				_, err = client.SendPhoto(&tbot.RequestSendPhoto{
					ChatID:  u.Message.Chat.ID,
					Caption: "take photo!",
					Photo:   tbot.NewFileURL("https://photolemur.com/uploads/blog/unnamed.jpg"),
				})
				assert.NoError(t, err)
			case "file":
				_, err = client.SendDocument(&tbot.RequestSendDocument{
					ChatID:   u.Message.Chat.ID,
					Caption:  "take file!",
					Document: tbot.NewFile("README.md", fileData),
				})
				assert.NoError(t, err)
			case "delete":
				err = client.DeleteMessage(&tbot.RequestDeleteMessage{
					ChatID:    u.Message.Chat.ID,
					MessageID: u.Message.ID,
				})
				assert.NoError(t, err)
			default:
				_, err = client.SendMessage(&tbot.RequestSendMessage{
					ChatID: u.Message.Chat.ID,
					Text:   u.Message.Text,
				})
				assert.NoError(t, err)
			}
		}
	}
}
