package tbot

import "sync"

type QueueStorage interface {
	GetChats() (ids []int64, err error)

	PeekUpdate(chatID int64) (*Update, error)
	PopUpdate(chatID int64) error

	PushUpdate(chatID int64, u *Update) error
}

func newQueueMemoryStorage() QueueStorage {
	return &QueueMemoryStorage{
		updatesByUsers: make(map[int64][]*Update),
	}
}

type QueueMemoryStorage struct {
	mx sync.Mutex

	updatesByUsers map[int64][]*Update
}

func (s *QueueMemoryStorage) GetChats() (ids []int64, err error) {
	return []int64{}, nil
}

func (s *QueueMemoryStorage) PeekUpdate(chatID int64) (*Update, error) {
	s.mx.Lock()
	defer s.mx.Unlock()
	if updates := s.updatesByUsers[chatID]; len(updates) > 0 {
		return updates[0], nil
	}
	return nil, nil
}

func (s *QueueMemoryStorage) PopUpdate(chatID int64) error {
	s.mx.Lock()
	defer s.mx.Unlock()
	if updates, ok := s.updatesByUsers[chatID]; ok {
		updates = updates[1:]
		s.updatesByUsers[chatID] = updates
		if len(updates) == 0 {
			delete(s.updatesByUsers, chatID)
		}
	}
	return nil
}

func (s *QueueMemoryStorage) PushUpdate(chatID int64, u *Update) error {
	s.mx.Lock()
	defer s.mx.Unlock()
	if updates, ok := s.updatesByUsers[chatID]; ok {
		s.updatesByUsers[chatID] = append(updates, u)
	} else {
		s.updatesByUsers[chatID] = []*Update{u}
	}
	return nil
}
