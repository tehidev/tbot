package tbot_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tehidev/go/tbot"
)

func TestPoller(t *testing.T) {
	accessToken := os.Getenv("POLLER_ACCESS_TOKEN")
	if accessToken == "" {
		t.SkipNow()
	}

	client, err := tbot.NewClient().
		WithAccessToken(accessToken).
		Build()
	assert.NoError(t, err)

	handler := new(testPollerHandler)

	poller, err := tbot.NewPoller().
		WithClient(client).
		WithHandler(handler).
		Start()
	assert.NoError(t, err)
	defer poller.Stop()

	time.Sleep(time.Minute * 10)
}

type testPollerHandler struct{}

func (p *testPollerHandler) ServeUpdate(u *tbot.Update) error {
	fmt.Println("poller.ServeUpdate", u.Message.Text)
	return nil
}

func (p *testPollerHandler) OnTBotError(err error) {
	fmt.Println("poller.OnTBotError", err)
}
