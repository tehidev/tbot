package tbot_test

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tehidev/go/tbot"
	"gitlab.com/tehidev/go/tbot/codes"
)

func TestRuntime(t *testing.T) {
	accessToken := os.Getenv("RUNTIME_ACCESS_TOKEN")
	if accessToken == "" {
		t.SkipNow()
	}

	client, err := tbot.NewClient().
		WithAccessToken(accessToken).
		Build()
	assert.NoError(t, err)

	errorHandler := func(err error) {
		log.Println("bot error", err)
	}

	runtime, err := tbot.NewRuntime().
		WithDescriptionFile("runtime_test_script.yml").
		WithDefaultLanguage(codes.English).
		WithErrorHandler(errorHandler).
		WithAction("typing_status", actionTypingStatus(client)).
		WithAction("send_message", actionSendMessage(client)).
		WithAction("select_user_language", actionSelectUserLanguage(client)).
		WithAction("set_commands_and_listen", actionSetCommandsAndListen(client)).
		WithAction("enter_number", actionEnterNumber(client)).
		WithAction("print_result", actionPrintResult(client)).
		Build()
	assert.NoError(t, err)
	defer runtime.Wait()

	queue, err := tbot.NewQueue().
		WithHandler(runtime).
		Start()
	assert.NoError(t, err)
	defer queue.Stop()

	poller, err := tbot.NewPoller().
		WithClient(client).
		WithHandler(queue).
		Start()
	assert.NoError(t, err)
	defer poller.Stop()

	time.Sleep(time.Minute * 10)
}

func actionTypingStatus(client *tbot.Client) tbot.Action {
	return func(ctx *tbot.Context) error {
		err := client.SendChatAction(&tbot.RequestSendChatAction{
			ChatID: ctx.ChatID,
			Action: tbot.ChatActionTyping,
		})
		if err != nil {
			return err
		}
		dur, err := ctx.ParamDuration("timeout")
		if err != nil {
			return err
		}
		time.Sleep(dur)

		return ctx.Next()
	}
}

func actionSendMessage(client *tbot.Client) tbot.Action {
	return func(ctx *tbot.Context) error {
		_, err := client.SendMessage(&tbot.RequestSendMessage{
			ChatID: ctx.ChatID,
			Text:   ctx.Param("text"),
		})
		if err != nil {
			return err
		}
		return ctx.Next()
	}
}

func actionSelectUserLanguage(client *tbot.Client) tbot.Action {
	const lastMsgKey = "last_language_msg_id"

	return func(ctx *tbot.Context) error {
		getMarkup := func() *tbot.InlineKeyboardMarkup {
			return &tbot.InlineKeyboardMarkup{InlineKeyboard: [][]*tbot.InlineKeyboardButton{{
				{Text: ctx.Param("en_text"), CallbackData: "en"},
				{Text: ctx.Param("ru_text"), CallbackData: "ru"},
			}}}
		}

		if ctx.Update == nil {
			if err := deleteChatCommands(client, ctx); err != nil {
				return err
			}
			msg, err := client.SendMessage(&tbot.RequestSendMessage{
				ChatID:      ctx.ChatID,
				Text:        ctx.Param("text"),
				ReplyMarkup: getMarkup(),
			})
			if err != nil {
				return err
			}
			return ctx.PutInt64(lastMsgKey, msg.ID)
		}

		if q := ctx.Update.CallbackQuery; q != nil {
			client.AnswerCallbackQuery(&tbot.RequestAnswerCallbackQuery{ID: q.ID})

			if lastMsgID, err := ctx.GetInt64(lastMsgKey); err == nil {
				client.EditMessageReplyMarkup(&tbot.RequestEditMessageReplyMarkup{
					ChatID:    ctx.ChatID,
					MessageID: lastMsgID,
					ReplyMarkup: getMarkup().UpdateText(q.Data, func(oldText string) (newText string) {
						return "✅ " + oldText
					}),
				})
			}

			switch q.Data {
			case "ru":
				ctx.Cursor.Language = codes.Russian
				return ctx.Next()
			case "en":
				ctx.Cursor.Language = codes.English
				return ctx.Next()
			}
		}

		return nil
	}
}

func actionEnterNumber(client *tbot.Client) tbot.Action {
	return func(ctx *tbot.Context) error {
		if ctx.Update == nil {
			if err := deleteChatCommands(client, ctx); err != nil {
				return err
			}
			_, err := client.SendMessage(&tbot.RequestSendMessage{
				ChatID: ctx.ChatID,
				Text:   ctx.Param("text"),
			})
			return err
		}
		if ctx.Update.Message != nil {
			number, err := strconv.Atoi(ctx.Update.Message.Text)
			if err != nil {
				_, err = client.SendMessage(&tbot.RequestSendMessage{
					ChatID: ctx.ChatID,
					Text:   ctx.Param("err_text"),
				})
				return err
			}
			if err = ctx.PutInt(ctx.Param("store_to"), number); err != nil {
				return err
			}
			return ctx.Next()
		}
		return nil
	}
}

func actionPrintResult(client *tbot.Client) tbot.Action {
	return func(ctx *tbot.Context) error {
		number1, err := ctx.GetInt(ctx.Param("where_1"))
		if err != nil {
			return err
		}
		number2, err := ctx.GetInt(ctx.Param("where_2"))
		if err != nil {
			return err
		}

		var result int

		action, err := ctx.GetString("action")
		if err != nil {
			return err
		}
		switch action {
		case "+":
			result = number1 + number2
		case "-":
			result = number1 - number2
		case "*":
			result = number1 * number2
		case "/":
			result = number1 / number2
		}

		_, err = client.SendMessage(&tbot.RequestSendMessage{
			ChatID: ctx.ChatID,
			Text:   fmt.Sprintf("%s: %d", ctx.Param("result_text"), result),
		})
		if err != nil {
			return err
		}

		return ctx.Next()
	}
}

func actionSetCommandsAndListen(client *tbot.Client) tbot.Action {
	getMarkup := func() *tbot.InlineKeyboardMarkup {
		return &tbot.InlineKeyboardMarkup{InlineKeyboard: [][]*tbot.InlineKeyboardButton{
			{
				{Text: "+", CallbackData: "+"},
				{Text: "-", CallbackData: "-"},
			},
			{
				{Text: "*", CallbackData: "*"},
				{Text: "/", CallbackData: "/"},
			},
		}}
	}

	const lastListenMsgKey = "last_listen_msg_id"

	return func(ctx *tbot.Context) error {
		if ctx.Update == nil {
			msg, err := client.SendMessage(&tbot.RequestSendMessage{
				ChatID:      ctx.ChatID,
				Text:        ctx.Param("msg_text"),
				ReplyMarkup: getMarkup(),
			})
			if err != nil {
				return err
			}
			if err = ctx.PutInt64(lastListenMsgKey, msg.ID); err != nil {
				return err
			}

			err = setChatCommands(client, ctx, []*tbot.BotCommand{
				{Command: ctx.Param("cmd_start"), Description: ctx.Param("cmd_start_desc")},
				{Command: ctx.Param("cmd_instruction"), Description: ctx.Param("cmd_instruction_desc")},
				{Command: ctx.Param("cmd_language"), Description: ctx.Param("cmd_language_desc")},
			})
			if err != nil {
				return err
			}

			err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
				ChatID: ctx.ChatID,
				MenuButton: &tbot.MenuButton{
					Type: tbot.MenuButtonCommands,
					Text: ctx.Param("cmd_btn_text"),
				},
			})
			return err
		}

		if q := ctx.Update.CallbackQuery; q != nil {
			client.AnswerCallbackQuery(&tbot.RequestAnswerCallbackQuery{ID: q.ID})

			switch q.Data {
			case "+", "-", "*", "/":
				if err := ctx.PutString("action", q.Data); err != nil {
					return err
				}
				if lastMsgID, err := ctx.GetInt64(lastListenMsgKey); err == nil {
					client.EditMessageReplyMarkup(&tbot.RequestEditMessageReplyMarkup{
						ChatID:    ctx.ChatID,
						MessageID: lastMsgID,
						ReplyMarkup: getMarkup().UpdateText(q.Data, func(oldText string) (newText string) {
							return "✅ " + oldText
						}),
					})
				}
				return ctx.JumpParam("on_input_numbers")
			}
		} else if msg := ctx.Update.Message; msg != nil {
			switch msg.Text {
			case ctx.Param("cmd_start"):
				return ctx.JumpParam("on_start")
			case ctx.Param("cmd_instruction"):
				return ctx.JumpParam("on_instruction")
			case ctx.Param("cmd_language"):
				return ctx.JumpParam("on_language")
			default:
				client.SendMessage(&tbot.RequestSendMessage{
					ChatID: ctx.ChatID,
					Text:   fmt.Sprintf("'%s' ?", msg.Text),
				})
			}
		}
		return nil
	}
}

func deleteChatCommands(client *tbot.Client, ctx *tbot.Context) error {
	return client.DeleteMyCommands(&tbot.RequestDeleteMyCommands{Scope: &tbot.BotCommandScope{
		ChatID: ctx.ChatID,
		Type:   tbot.BotCommandScopeTypeChat,
	}})
}

func setChatCommands(client *tbot.Client, ctx *tbot.Context, commands []*tbot.BotCommand) error {
	return client.SetMyCommands(&tbot.RequestSetMyCommands{
		Scope: &tbot.BotCommandScope{
			ChatID: ctx.ChatID,
			Type:   tbot.BotCommandScopeTypeChat,
		},
		Commands: commands,
	})
}
