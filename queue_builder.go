package tbot

import "errors"

func NewQueue() *QueueBuilder {
	return &QueueBuilder{&Queue{
		chatsWorkers: make(map[int64]struct{}),
		done:         make(chan struct{}),
		storage:      newQueueMemoryStorage(),
	}}
}

type QueueBuilder struct {
	q *Queue
}

func (b *QueueBuilder) WithStorage(storage QueueStorage) *QueueBuilder {
	b.q.storage = storage
	return b
}

func (b *QueueBuilder) WithHandler(handler BotHandler) *QueueBuilder {
	b.q.handler = handler
	return b
}

func (b *QueueBuilder) Start() (*Queue, error) {
	switch {
	case b.q.storage == nil:
		return nil, errors.New("queue.storage is no set")
	case b.q.handler == nil:
		return nil, errors.New("queue.handler is no set")
	}
	return b.q, b.q.fetchUpdates()
}
