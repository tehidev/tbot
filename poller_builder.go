package tbot

import (
	"errors"
	"time"
)

func NewPoller() *PollerBuilder {
	return &PollerBuilder{&Poller{
		done:        false,
		stop:        make(chan struct{}),
		storage:     newPollerEmptyStorage(),
		pollLimit:   100,
		pollTimeout: time.Second * 10,
		failTimeout: time.Second * 3,
	}}
}

type PollerBuilder struct {
	p *Poller
}

func (b *PollerBuilder) WithPollLimit(value int64) *PollerBuilder {
	b.p.pollLimit = value
	return b
}

func (b *PollerBuilder) WithPollTimeout(value time.Duration) *PollerBuilder {
	b.p.pollTimeout = value
	return b
}

func (b *PollerBuilder) WithFailTimeout(value time.Duration) *PollerBuilder {
	b.p.failTimeout = value
	return b
}

func (b *PollerBuilder) WithClient(client BotClient) *PollerBuilder {
	b.p.client = client
	return b
}

func (b *PollerBuilder) WithStorage(storage PollerStorage) *PollerBuilder {
	b.p.storage = storage
	return b
}

func (b *PollerBuilder) WithHandler(handler BotHandler) *PollerBuilder {
	b.p.handler = handler
	return b
}

func (b *PollerBuilder) Start() (*Poller, error) {
	switch {
	case b.p.client == nil:
		return nil, errors.New("poller.Client is no set")
	case b.p.storage == nil:
		return nil, errors.New("poller.Storage is no set")
	case b.p.handler == nil:
		return nil, errors.New("poller.Handler is no set")
	case b.p.pollLimit == 0:
		return nil, errors.New("poller.PollLimit is no set")
	case b.p.pollTimeout < 0:
		return nil, errors.New("poller.PollTimeout is negative")
	case b.p.failTimeout < 0:
		return nil, errors.New("poller.FailTimeout is negative")
	}
	return b.p, b.p.start()
}
