package tbot

type BotHandler interface {
	ServeUpdate(*Update) error
	OnTBotError(error)
}

type BotClient interface {
	GetUpdates(*RequestGetUpdates) ([]*Update, error)
}
