package tbot

// A simple method for testing your bot's authentication token.
// Returns basic information about the bot in form of a User object.
func (c *Client) GetMe() (*User, error) {
	var res User
	return &res, c.Do("getMe", nil, &res)
}

// Use this method to log out from the cloud Bot API server before launching the bot locally.
// You must log out the bot before running it locally, otherwise there is no guarantee that the bot will receive updates.
// After a successful call, you can immediately log in on a local server,
// but will not be able to log in back to the cloud Bot API server for 10 minutes
func (c *Client) LogOut() error {
	return c.Do("logOut", nil, nil)
}

// Use this method to close the bot instance before moving it from one local server to another.
// You need to delete the webhook before calling this method to ensure that the bot isn't launched again after server restart.
// The method will return error 429 in the first 10 minutes after the bot is launched. Returns True on success.
func (c *Client) Close() error {
	return c.Do("close", nil, nil)
}

func (c *Client) GetUpdates(req *RequestGetUpdates) ([]*Update, error) {
	var res []*Update
	return res, c.Do("getUpdates", req, &res)
}

func (c *Client) SendMessage(req *RequestSendMessage) (*Message, error) {
	var res Message
	return &res, c.Do("sendMessage", req, &res)
}

func (c *Client) ForwardMessage(req *RequestForwardMessage) (*Message, error) {
	var res Message
	return &res, c.Do("forwardMessage", req, &res)
}

func (c *Client) CopyMessage(req *RequestCopyMessage) (MessageID, error) {
	var res MessageID
	return res, c.Do("copyMessage", req, &res)
}

func (c *Client) SendPhoto(req *RequestSendPhoto) (*Message, error) {
	// may be with files
	var res Message
	return &res, c.Do("sendPhoto", req, &res)
}

func (c *Client) SendAudio(req *RequestSendAudio) (*Message, error) {
	// may be with files
	var res Message
	return &res, c.Do("sendAudio", req, &res)
}

func (c *Client) SendDocument(req *RequestSendDocument) (*Message, error) {
	// may be with files
	var res Message
	return &res, c.Do("sendDocument", req, &res)
}

func (c *Client) SendVideo(req *RequestSendVideo) (*Message, error) {
	// may be with files
	var res Message
	return &res, c.Do("sendVideo", req, &res)
}

func (c *Client) SendAnimation(req *RequestSendAnimation) (*Message, error) {
	var res Message
	return &res, c.Do("sendAnimation", req, &res)
}

func (c *Client) SendVoice(req *RequestSendVoice) (*Message, error) {
	var res Message
	return &res, c.Do("sendVoice", req, &res)
}

func (c *Client) SendVideoNote(req *RequestSendVideoNote) (*Message, error) {
	var res Message
	return &res, c.Do("sendVideoNote", req, &res)
}

func (c *Client) SendMediaGroup(req *RequestSendMediaGroup) ([]*Message, error) {
	for _, item := range req.Media {
		item.SetInputMediaType()
	}
	var res []*Message
	return res, c.Do("sendMediaGroup", req, &res)
}

func (c *Client) SendLocation(req *RequestSendLocation) (*Message, error) {
	var res Message
	return &res, c.Do("sendLocation", req, &res)
}

func (c *Client) EditMessageLiveLocation(req *RequestEditMessageLiveLocation) (*Message, error) {
	var res Message
	return &res, c.Do("editMessageLiveLocation", req, &res)
}

func (c *Client) StopMessageLiveLocation(req *RequestStopMessageLiveLocation) (*Message, error) {
	var res Message
	return &res, c.Do("stopMessageLiveLocation", req, &res)
}

func (c *Client) SendVenue(req *RequestSendVenue) (*Message, error) {
	var res Message
	return &res, c.Do("sendVenue", req, &res)
}

func (c *Client) SendContact(req *RequestSendContact) (*Message, error) {
	var res Message
	return &res, c.Do("sendContact", req, &res)
}

func (c *Client) SendPoll(req *RequestSendPoll) (*Message, error) {
	var res Message
	return &res, c.Do("sendPoll", req, &res)
}

func (c *Client) SendDice(req *RequestSendDice) (*Message, error) {
	var res Message
	return &res, c.Do("sendDice", req, &res)
}

func (c *Client) SendChatAction(req *RequestSendChatAction) error {
	return c.Do("sendChatAction", req, nil)
}

func (c *Client) GetUserProfilePhotos(req *RequestGetUserProfilePhotos) (UserProfilePhotos, error) {
	var res UserProfilePhotos
	return res, c.Do("getUserProfilePhotos", req, &res)
}

func (c *Client) GetFile(req *RequestGetFile) (*File, error) {
	var res File
	return &res, c.Do("getFile", req, &res)
}

func (c *Client) GetFileLink(req *RequestGetFile) (link string, err error) {
	file, err := c.GetFile(req)
	link = c.endPoint + "/file/bot" + c.accessToken + "/" + file.FilePath
	return link, err
}

func (c *Client) BanChatMember(req *RequestBanChatMember) error {
	return c.Do("banChatMember", req, nil)
}

func (c *Client) UnbanChatMember(req *RequestUnbanChatMember) error {
	return c.Do("unbanChatMember", req, nil)
}

func (c *Client) RestrictChatMember(req *RequestRestrictChatMember) error {
	return c.Do("restrictChatMember", req, nil)
}

func (c *Client) PromoteChatMember(req *RequestPromoteChatMember) error {
	return c.Do("promoteChatMember", req, nil)
}

func (c *Client) SetChatAdministratorCustomTitle(req *RequestSetChatAdministratorCustomTitle) error {
	return c.Do("setChatAdministratorCustomTitle", req, nil)
}

func (c *Client) BanChatSenderChat(req *RequestBanChatSenderChat) error {
	return c.Do("banChatSenderChat", req, nil)
}

func (c *Client) UnbanChatSenderChat(req *RequestUnbanChatSenderChat) error {
	return c.Do("unbanChatSenderChat", req, nil)
}

func (c *Client) SetChatPermissions(req *RequestSetChatPermissions) error {
	return c.Do("setChatPermissions", req, nil)
}

func (c *Client) ExportChatInviteLink(req *RequestExportChatInviteLink) (link string, err error) {
	return link, c.Do("exportChatInviteLink", req, &link)
}

func (c *Client) CreateChatInviteLink(req *RequestCreateChatInviteLink) (*ChatInviteLink, error) {
	var res ChatInviteLink
	return &res, c.Do("createChatInviteLink", req, &res)
}

func (c *Client) EditChatInviteLink(req *RequestEditChatInviteLink) (*ChatInviteLink, error) {
	var res ChatInviteLink
	return &res, c.Do("editChatInviteLink", req, &res)
}

func (c *Client) RevokeChatInviteLink(req *RequestRevokeChatInviteLink) (*ChatInviteLink, error) {
	var res ChatInviteLink
	return &res, c.Do("revokeChatInviteLink", req, &res)
}

func (c *Client) ApproveChatJoinRequest(req *RequestApproveChatJoinRequest) error {
	return c.Do("approveChatJoinRequest", req, nil)
}

func (c *Client) DeclineChatJoinRequest(req *RequestDeclineChatJoinRequest) error {
	return c.Do("declineChatJoinRequest", req, nil)
}

func (c *Client) SetChatPhoto(req *RequestSetChatPhoto) error {
	return c.Do("setChatPhoto", req, nil)
}

func (c *Client) DeleteChatPhoto(req *RequestDeleteChatPhoto) error {
	return c.Do("deleteChatPhoto", req, nil)
}

func (c *Client) SetChatTitle(req *RequestSetChatTitle) error {
	return c.Do("setChatTitle", req, nil)
}

func (c *Client) SetChatDescription(req *RequestSetChatDescription) error {
	return c.Do("setChatDescription", req, nil)
}

func (c *Client) PinChatMessage(req *RequestPinChatMessage) error {
	return c.Do("pinChatMessage", req, nil)
}

func (c *Client) UnpinChatMessage(req *RequestUnpinChatMessage) error {
	return c.Do("unpinChatMessage", req, nil)
}

func (c *Client) UnpinAllChatMessages(req *RequestUnpinAllChatMessages) error {
	return c.Do("unpinAllChatMessages", req, nil)
}

func (c *Client) LeaveChat(req *RequestLeaveChat) error {
	return c.Do("leaveChat", req, nil)
}

func (c *Client) GetChat(req *RequestGetChat) (*Chat, error) {
	var res Chat
	return &res, c.Do("getChat", req, &res)
}

func (c *Client) GetChatAdministrators(req *RequestGetChatAdministrators) ([]*ChatMember, error) {
	var res []*ChatMember
	return res, c.Do("getChatAdministrators", req, &res)
}

func (c *Client) GetChatMemberCount(req *RequestGetChatMemberCount) (int64, error) {
	var res int64
	return res, c.Do("getChatMemberCount", req, &res)
}

func (c *Client) GetChatMember(req *RequestGetChatMember) (*ChatMember, error) {
	var res ChatMember
	return &res, c.Do("getChatMember", req, &res)
}

func (c *Client) SetChatStickerSet(req *RequestSetChatStickerSet) error {
	return c.Do("setChatStickerSet", req, nil)
}

func (c *Client) DeleteChatStickerSet(req *RequestDeleteChatStickerSet) error {
	return c.Do("deleteChatStickerSet", req, nil)
}

func (c *Client) AnswerCallbackQuery(req *RequestAnswerCallbackQuery) error {
	return c.Do("answerCallbackQuery", req, nil)
}

func (c *Client) AnswerCallbackByUpdate(u *Update) error {
	return c.AnswerCallbackQuery(&RequestAnswerCallbackQuery{
		ID: u.CallbackQuery.ID,
	})
}

func (c *Client) SetMyCommands(req *RequestSetMyCommands) error {
	return c.Do("setMyCommands", req, nil)
}

func (c *Client) DeleteMyCommands(req *RequestDeleteMyCommands) error {
	return c.Do("deleteMyCommands", req, nil)
}

func (c *Client) GetMyCommands(req *RequestGetMyCommands) ([]*BotCommand, error) {
	var res []*BotCommand
	return res, c.Do("getMyCommands", req, &res)
}

func (c *Client) SetChatMenuButton(req *RequestSetChatMenuButton) error {
	return c.Do("setChatMenuButton", req, nil)
}

func (c *Client) GetChatMenuButton(req *RequestGetChatMenuButton) (*MenuButton, error) {
	var res MenuButton
	return &res, c.Do("getChatMenuButton", req, &res)
}

func (c *Client) SetMyDefaultAdministratorRights(req *RequestSetMyDefaultAdministratorRights) error {
	return c.Do("setMyDefaultAdministratorRights", req, nil)
}

func (c *Client) GetMyDefaultAdministratorRights(req *RequestGetMyDefaultAdministratorRights) (*ChatAdministratorRights, error) {
	var res ChatAdministratorRights
	return &res, c.Do("getMyDefaultAdministratorRights", req, &res)
}

func (c *Client) EditMessageText(req *RequestEditMessageText) (*Message, error) {
	var res Message
	return &res, c.Do("editMessageText", req, &res)
}

func (c *Client) EditMessageCaption(req *RequestEditMessageCaption) (*Message, error) {
	var res Message
	return &res, c.Do("editMessageCaption", req, &res)
}

func (c *Client) EditMessageMedia(req *RequestEditMessageMedia) (*Message, error) {
	req.Media.SetInputMediaType()
	var res Message
	return &res, c.Do("editMessageMedia", req, &res)
}

func (c *Client) EditMessageReplyMarkup(req *RequestEditMessageReplyMarkup) (*Message, error) {
	var res Message
	return &res, c.Do("editMessageReplyMarkup", req, &res)
}

func (c *Client) StopPoll(req *RequestStopPoll) (*Poll, error) {
	var res Poll
	return &res, c.Do("stopPoll", req, &res)
}

func (c *Client) DeleteMessage(req *RequestDeleteMessage) error {
	return c.Do("deleteMessage", req, nil)
}

func (c *Client) SendAnswerWebAppQuery(req *RequestAnswerWebAppQuery) (*SentWebAppMessage, error) {
	var res SentWebAppMessage
	return &res, c.Do("answerWebAppQuery", req, &res)
}

func (c *Client) SendInvoice(req *RequestSendInvoice) (*Message, error) {
	var res Message
	return &res, c.Do("sendInvoice", req, &res)
}

func (c *Client) CreateInvoiceLink(req *RequestCreateInvoiceLink) (link string, err error) {
	return link, c.Do("createInvoiceLink", req, &link)
}

func (c *Client) AnswerShippingQuery(req *RequestAnswerShippingQuery) error {
	return c.Do("answerShippingQuery", req, nil)
}

func (c *Client) AnswerPreCheckoutQuery(req *RequestAnswerPreCheckoutQuery) error {
	return c.Do("answerPreCheckoutQuery", req, nil)
}

func (c *Client) SetPassportDataErrors(req *RequestSetPassportDataErrors) error {
	return c.Do("setPassportDataErrors", req, nil)
}

func (c *Client) SendGame(req *RequestSendGame) (*Message, error) {
	var res Message
	return &res, c.Do("sendGame", req, &res)
}
