package tbot

import (
	"context"
	"errors"
	"time"
)

type Poller struct {
	done bool
	stop chan struct{}

	client BotClient

	storage PollerStorage
	handler BotHandler

	pollLimit   int64
	pollTimeout time.Duration
	failTimeout time.Duration
}

func (p *Poller) start() error {
	lastUpdate, err := p.storage.GetLastUpdate()
	if err != nil {
		return err
	}

	go p.startListenUpdates(lastUpdate)

	return nil
}

func (p *Poller) Stop() {
	p.done = true
	<-p.stop
}

func (p *Poller) startListenUpdates(lastUpdate int64) {
	defer close(p.stop)

	updateRequest := &RequestGetUpdates{
		Timeout: int64(p.pollTimeout / time.Second),
		Limit:   p.pollLimit,
	}

	for !p.done {
		updateRequest.Offset = lastUpdate + 1

		updates, err := p.client.GetUpdates(updateRequest)
		if err != nil {
			if errors.Is(err, context.Canceled) {
				break
			}
			p.handler.OnTBotError(err)
			time.Sleep(p.failTimeout)
			continue
		}

		for _, u := range updates {
			if p.done {
				break
			}
			if err = p.handler.ServeUpdate(u); err != nil {
				p.handler.OnTBotError(err)
				time.Sleep(p.failTimeout)
				break
			}
			if u.ID > lastUpdate {
				lastUpdate = u.ID
				if err = p.storage.PutLastUpdate(lastUpdate); err != nil {
					p.handler.OnTBotError(err)
				}
			}
		}
	}
}
