package tbot

type RuntimeStorage interface {
	GetCursor(chatID int64) (*Cursor, error)
	DelCursor(chatID int64) error
	PutCursor(*Cursor) error

	GetString(chatID int64, key string) (string, error)
	PutString(chatID int64, key, val string) error
	DelString(chatID int64, key string) error
}

func newRuntimeMemoryStorage() RuntimeStorage {
	return &runtimeMemoryStorage{
		strings: make(map[runtimeMemoryKey]string),
		cursors: make(map[int64]*Cursor),
	}
}

type runtimeMemoryStorage struct {
	strings map[runtimeMemoryKey]string
	cursors map[int64]*Cursor
}

type runtimeMemoryKey struct {
	ChatID int64
	Key    string
}

func (s *runtimeMemoryStorage) GetCursor(chatID int64) (*Cursor, error) {
	return s.cursors[chatID], nil
}

func (s *runtimeMemoryStorage) DelCursor(chatID int64) error {
	delete(s.cursors, chatID)
	return nil
}

func (s *runtimeMemoryStorage) PutCursor(cursor *Cursor) error {
	s.cursors[cursor.ChatID] = cursor
	return nil
}

func (s *runtimeMemoryStorage) GetString(chatID int64, key string) (string, error) {
	return s.strings[runtimeMemoryKey{chatID, key}], nil
}

func (s *runtimeMemoryStorage) PutString(chatID int64, key, val string) error {
	s.strings[runtimeMemoryKey{chatID, key}] = val
	return nil
}

func (s *runtimeMemoryStorage) DelString(chatID int64, key string) error {
	delete(s.strings, runtimeMemoryKey{chatID, key})
	return nil
}
