# tBot - telegram bot processing library

### A lot of components for telegram bot design, includes:

- [some types](#some-types)
- [tbot.Client](#tbotclient-example) - executing API requests
- [tbot.Poller](#tbotpoller-example) - receiving long-poll updates
- [tbot.Queue](#tbotqueue-example) - splitting updates by chat and execution in goroutine
- [tbot.Runtime](#tbotruntime-example) - processing chat flow by script file
  - [example script file](#tbotruntime-example-script-file)

### Pretty chat flow processing

tbot.Client -> tbot.Poller -> tbot.Queue -> tbot.Runtime

## some types

```go
type Language string // package "tbot/codes"

type Cursor struct {
    ChatID int64 `json:"chatID"`

    Language Language `json:"language"`

    BranchName string `json:"branchName"`
    BranchLine int    `json:"branchLine"`
    ActionName string `json:"actionName"`
}

type Context struct {
    *Cursor

    Update *Update
    Params HandlerParams
}

type Handler func(*Context) error

type HandlerParams map[string]string

// errors:

type ErrClient struct {
    Err error

    Code    int
    Method  string
    Details string
}

type ErrRuntime struct {
    Err    error
    Cursor *Cursor
    Update *Update
}
```

## tbot.Client example

```go
package main

import (
    "io/ioutil"
    "os"

    "gitlab.com/tehidev/go/tbot"
)

func main() {
    client, err := tbot.NewClient().
        // WithEndPoint("https://api.telegram.org").
        // WithHTTPClient(http.DefaultClient).
        WithAccessToken(os.Getenv("ACCESS_TOKEN")).
        Build()
    check(err)
    defer client.Stop()

    fileData, err := ioutil.ReadFile("README.md")
    check(err)

    var offset int64 = 0
    for {
        updates, err := client.GetUpdates(&tbot.RequestGetUpdates{
            Offset:  offset + 1,
            Limit:   100,
            Timeout: 10,
        })
        check(err)

        for _, u := range updates {
            if u.UpdateID > offset {
                offset = u.UpdateID
            }

            if u.Message == nil {
                continue
            }

            println("offset", offset, "chat_id", u.Message.Chat.ID)

            switch u.Message.Text {
            case "board":
                _, err = client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: u.Message.Chat.ID,
                    Text:   "Держи keyboard",
                    ReplyMarkup: tbot.ReplyKeyboardMarkup{
                        Keyboard: [][]*tbot.KeyboardButton{
                            {
                                {
                                    Text: "enter to google base",
                                    WebApp: &tbot.WebAppInfo{
                                        URL: "https://google.com",
                                    },
                                },
                            },
                        },
                    },
                })
                check(err)
            case "menu":
                _, err = client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: u.Message.Chat.ID,
                    Text:   "Держи кнопка-меню",
                    ReplyMarkup: tbot.InlineKeyboardMarkup{
                        InlineKeyboard: [][]*tbot.InlineKeyboardButton{
                            {
                                {
                                    Text: "enter to google app",
                                    WebApp: &tbot.WebAppInfo{
                                        URL: "https://www.google.am/search?q=kek&source=lnms&tbm=isch",
                                    },
                                },
                            },
                        },
                    },
                })
                check(err)
            case "off":
                err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
                    ChatID: u.Message.Chat.ID,
                })
                check(err)
                _, err = client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: u.Message.Chat.ID,
                    Text:   "off",
                    ReplyMarkup: tbot.ReplyKeyboardRemove{
                        RemoveKeyboard: true,
                    },
                })
                check(err)
            case "on":
                err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
                    ChatID: u.Message.Chat.ID,
                    MenuButton: &tbot.MenuButton{
                        Type: tbot.MenuButtonWebApp,
                        Text: "Enter to yandex app",
                        WebApp: &tbot.WebAppInfo{
                            URL: "https://ya.ru",
                        },
                    },
                })
                check(err)
            case "photo":
                _, err = client.SendPhoto(&tbot.RequestSendPhoto{
                    ChatID:  u.Message.Chat.ID,
                    Caption: "take photo!",
                    Photo:   tbot.NewFileURL("https://photolemur.com/uploads/blog/unnamed.jpg"),
                })
                check(err)
            case "file":
                _, err = client.SendDocument(&tbot.RequestSendDocument{
                    ChatID:   u.Message.Chat.ID,
                    Caption:  "take file!",
                    Document: tbot.NewFile("README.md", fileData),
                })
                check(err)
            case "delete":
                err = client.DeleteMessage(&tbot.RequestDeleteMessage{
                    ChatID:    u.Message.Chat.ID,
                    MessageID: u.Message.ID,
                })
                check(err)
            default:
                _, err = client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: u.Message.Chat.ID,
                    Text:   u.Message.Text,
                })
                check(err)
            }
        }
    }
}
```

## tbot.Poller example

```go
package main

import (
    "fmt"
    "os"

    "gitlab.com/tehidev/go/tbot"
)

func main() {
    client, err := tbot.NewClient().
        WithAccessToken(os.Getenv("ACCESS_TOKEN")).
        Build()
    check(err)

    // your implementation PollerStorage:
    //
    // - GetLastUpdate() (int64, error)
    // - PutLastUpdate(id int64)
    //
    // we are recommended using RedisDB
    // by default used in-memory storage
    //
    storage := newStorage()

    handler := new(testPollerHandler)

    poller, err := tbot.NewPoller().
        // WithFailTimeout(time.Second * 3).
        // WithPollTimeout(time.Second * 10).
        // WithPollLimit(100).
        // WithStorage(storage).
        WithClient(client).
        WithHandler(handler).
        Start()
    check(err)
    defer poller.Stop()

    select {}
}

type testPollerHandler struct {}

func (p *testPollerHandler) ServeUpdate(u *tbot.Update) error {
    fmt.Println("poller.ServeUpdate", u.Message.Text)
    return nil
}

func (p *testPollerHandler) OnTBotError(err error) {
    fmt.Println("poller.OnTBotError", err)
}
```

## tbot.Queue example

```go
package main

import (
    "fmt"
    "os"
    "time"

    "gitlab.com/tehidev/go/tbot"
)

func main() {
    client, err := tbot.NewClient().
        WithAccessToken(os.Getenv("ACCESS_TOKEN")).
        Build()
    check(err)

    // your implementation QueueStorage:
    //
    // - GetChats() (ids []int64, err error)
    // - PeekUpdate(chatID int64) (*Update, error)
    // - PopUpdate(chatID int64) error
    // - PushUpdate(chatID int64, u *Update) error
    //
    // we are recommended using any Relation DB or Message Queuing Manager or RedisDB
    // by default used in-memory storage
    //
    storage := newStorage()

    handler := new(testQueueHandler)

    queue, err := tbot.NewQueue().
        WithHandler(handler).
        WithStorage(storage).
        Start()
    check(err)
    defer queue.Stop()

    poller, err := tbot.NewPoller().
        WithClient(client).
        WithHandler(queue).
        Start()
    check(err)
    defer poller.Stop()

    go func() {
        for {
            println("queue.WorkersCount", queue.WorkersCount())
            time.Sleep(time.Second)
        }
    }()

    select {}
}

type testQueueHandler struct {}

func (q *testQueueHandler) ServeUpdate(u *tbot.Update) error {
    fmt.Printf("queue.ServeUpdate user(%d) text(%s)\n", u.User().ID, u.Message.Text)
    time.Sleep(time.Second * 2)
    return nil
}

func (q *testQueueHandler) OnTBotError(err error) {
    fmt.Println("queue.OnTBotError", err)
}
```

## tbot.Runtime example

```go
package main

import (
    "fmt"
    "log"
    "os"
    "strconv"
    "time"

    "gitlab.com/tehidev/go/tbot"
    "gitlab.com/tehidev/go/tbot/codes"
)

func main() {
    client, err := tbot.NewClient().
        WithAccessToken(os.Getenv("ACCESS_TOKEN")).
        Build()
    check(err)

    errorHandler := func(err error) {
        log.Println("bot error", err)
        // _, ok := err.(*tbot.ErrClient)
        // _, ok := err.(*tbot.ErrRuntime)
    }

    // your implementation RuntimeStorage:
    //
    // - GetCursor(chatID int64) (*Cursor, error)
    // - DelCursor(chatID int64) error
    // - PutCursor(*Cursor) error
    //
    // - GetString(chatID int64, key string) (string, error)
    // - PutString(chatID int64, key, val string) error
    // - DelString(chatID int64, key string) error
    //
    // we are recommended using RedisDB
    // by default used in-memory storage
    //
    storage := newStorage()

    runtime, err := tbot.NewRuntime().
        // WithStorage(storage).
        // WithDescriptionData([]byte("yaml file data")).
        // WithContext(context.Background()).
        WithDescriptionFile("runtime_test_script.yml").
        WithDefaultLanguage(codes.English).
        WithErrorHandler(errorHandler).
        WithAction("typing_status", actionTypingStatus(client)).
        WithAction("send_message", actionSendMessage(client)).
        WithAction("select_user_language", actionSelectUserLanguage(client)).
        WithAction("set_commands_and_listen", actionSetCommandsAndListen(client)).
        WithAction("enter_number", actionEnterNumber(client)).
        WithAction("print_result", actionPrintResult(client)).
        Build()
    check(err)
    defer runtime.Wait()

    queue, err := tbot.NewQueue().
        WithHandler(runtime).
        Start()
    check(err)
    defer queue.Stop()

    poller, err := tbot.NewPoller().
        WithClient(client).
        WithHandler(queue).
        Start()
    check(err)
    defer poller.Stop()

    time.Sleep(time.Minute * 10)
}

func actionTypingStatus(client *tbot.Client) tbot.Action {
    return func(ctx *tbot.Context) error {
        err := client.SendChatAction(&tbot.RequestSendChatAction{
            ChatID: ctx.ChatID,
            Action: tbot.ChatActionTyping,
        })
        if err != nil {
            return err
        }
        dur, err := ctx.ParamDuration("timeout")
        if err != nil {
            return err
        }
        time.Sleep(dur)

        return ctx.Next()
    }
}

func actionSendMessage(client *tbot.Client) tbot.Action {
    return func(ctx *tbot.Context) error {
        _, err := client.SendMessage(&tbot.RequestSendMessage{
            ChatID: ctx.ChatID,
            Text:   ctx.Param("text"),
        })
        if err != nil {
            return err
        }
        return ctx.Next()
    }
}

func actionSelectUserLanguage(client *tbot.Client) tbot.Action {
    const lastMsgKey = "last_language_msg_id"

    return func(ctx *tbot.Context) error {
        getMarkup := func() *tbot.InlineKeyboardMarkup {
            return &tbot.InlineKeyboardMarkup{InlineKeyboard: [][]*tbot.InlineKeyboardButton{{
                {Text: ctx.Param("en_text"), CallbackData: "en"},
                {Text: ctx.Param("ru_text"), CallbackData: "ru"},
            }}}
        }

        if ctx.Update == nil {
            if err := deleteChatCommands(client, ctx); err != nil {
                return err
            }
            msg, err := client.SendMessage(&tbot.RequestSendMessage{
                ChatID:      ctx.ChatID,
                Text:        ctx.Param("text"),
                ReplyMarkup: getMarkup(),
            })
            if err != nil {
                return err
            }
            return ctx.PutInt64(lastMsgKey, msg.ID)
        }

        if q := ctx.Update.CallbackQuery; q != nil {
            client.AnswerCallbackQuery(&tbot.RequestAnswerCallbackQuery{ID: q.ID})

            if lastMsgID, err := ctx.GetInt64(lastMsgKey); err == nil {
                client.EditMessageReplyMarkup(&tbot.RequestEditMessageReplyMarkup{
                    ChatID:    ctx.ChatID,
                    MessageID: lastMsgID,
                    ReplyMarkup: getMarkup().UpdateText(q.Data, func(oldText string) (newText string) {
                        return "✅ " + oldText
                    }),
                })
            }

            switch q.Data {
            case "ru":
                ctx.Cursor.Language = codes.Russian
                return ctx.Next()
            case "en":
                ctx.Cursor.Language = codes.English
                return ctx.Next()
            }
        }

        return nil
    }
}

func actionEnterNumber(client *tbot.Client) tbot.Action {
    return func(ctx *tbot.Context) error {
        if ctx.Update == nil {
            if err := deleteChatCommands(client, ctx); err != nil {
                return err
            }
            _, err := client.SendMessage(&tbot.RequestSendMessage{
                ChatID: ctx.ChatID,
                Text:   ctx.Param("text"),
            })
            return err
        }
        if ctx.Update.Message != nil {
            number, err := strconv.Atoi(ctx.Update.Message.Text)
            if err != nil {
                _, err = client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: ctx.ChatID,
                    Text:   ctx.Param("err_text"),
                })
                return err
            }
            if err = ctx.PutInt(ctx.Param("store_to"), number); err != nil {
                return err
            }
            return ctx.Next()
        }
        return nil
    }
}

func actionPrintResult(client *tbot.Client) tbot.Action {
    return func(ctx *tbot.Context) error {
        number1, err := ctx.GetInt(ctx.Param("where_1"))
        if err != nil {
            return err
        }
        number2, err := ctx.GetInt(ctx.Param("where_2"))
        if err != nil {
            return err
        }

        var result int

        action, err := ctx.GetString("action")
        if err != nil {
            return err
        }
        switch action {
        case "+":
            result = number1 + number2
        case "-":
            result = number1 - number2
        case "*":
            result = number1 * number2
        case "/":
            result = number1 / number2
        }

        _, err = client.SendMessage(&tbot.RequestSendMessage{
            ChatID: ctx.ChatID,
            Text:   fmt.Sprintf("%s: %d", ctx.Param("result_text"), result),
        })
        if err != nil {
            return err
        }

        return ctx.Next()
    }
}

func actionSetCommandsAndListen(client *tbot.Client) tbot.Action {
    getMarkup := func() *tbot.InlineKeyboardMarkup {
        return &tbot.InlineKeyboardMarkup{InlineKeyboard: [][]*tbot.InlineKeyboardButton{
            {
                {Text: "+", CallbackData: "+"},
                {Text: "-", CallbackData: "-"},
            },
            {
                {Text: "*", CallbackData: "*"},
                {Text: "/", CallbackData: "/"},
            },
        }}
    }

    const lastListenMsgKey = "last_listen_msg_id"

    return func(ctx *tbot.Context) error {
        if ctx.Update == nil {
            msg, err := client.SendMessage(&tbot.RequestSendMessage{
                ChatID:      ctx.ChatID,
                Text:        ctx.Param("msg_text"),
                ReplyMarkup: getMarkup(),
            })
            if err != nil {
                return err
            }
            if err = ctx.PutInt64(lastListenMsgKey, msg.ID); err != nil {
                return err
            }

            err = setChatCommands(client, ctx, []*tbot.BotCommand{
                {Command: ctx.Param("cmd_start"), Description: ctx.Param("cmd_start_desc")},
                {Command: ctx.Param("cmd_instruction"), Description: ctx.Param("cmd_instruction_desc")},
                {Command: ctx.Param("cmd_language"), Description: ctx.Param("cmd_language_desc")},
            })
            if err != nil {
                return err
            }

            err = client.SetChatMenuButton(&tbot.RequestSetChatMenuButton{
                ChatID: ctx.ChatID,
                MenuButton: &tbot.MenuButton{
                    Type: tbot.MenuButtonCommands,
                    Text: ctx.Param("cmd_btn_text"),
                },
            })
            return err
        }

        if q := ctx.Update.CallbackQuery; q != nil {
            client.AnswerCallbackQuery(&tbot.RequestAnswerCallbackQuery{ID: q.ID})

            switch q.Data {
            case "+", "-", "*", "/":
                if err := ctx.PutString("action", q.Data); err != nil {
                    return err
                }
                if lastMsgID, err := ctx.GetInt64(lastListenMsgKey); err == nil {
                    client.EditMessageReplyMarkup(&tbot.RequestEditMessageReplyMarkup{
                        ChatID:    ctx.ChatID,
                        MessageID: lastMsgID,
                        ReplyMarkup: getMarkup().UpdateText(q.Data, func(oldText string) (newText string) {
                            return "✅ " + oldText
                        }),
                    })
                }
                return ctx.JumpParam("on_input_numbers")
            }
        } else if msg := ctx.Update.Message; msg != nil {
            switch msg.Text {
            case ctx.Param("cmd_start"):
                return ctx.JumpParam("on_start")
            case ctx.Param("cmd_instruction"):
                return ctx.JumpParam("on_instruction")
            case ctx.Param("cmd_language"):
                return ctx.JumpParam("on_language")
            default:
                client.SendMessage(&tbot.RequestSendMessage{
                    ChatID: ctx.ChatID,
                    Text:   fmt.Sprintf("'%s' ?", msg.Text),
                })
            }
        }
        return nil
    }
}

func deleteChatCommands(client *tbot.Client, ctx *tbot.Context) error {
	return client.DeleteMyCommands(&tbot.RequestDeleteMyCommands{Scope: &tbot.BotCommandScope{
		ChatID: ctx.ChatID,
		Type:   tbot.BotCommandScopeTypeChat,
	}})
}

func setChatCommands(client *tbot.Client, ctx *tbot.Context, commands []*tbot.BotCommand) error {
	return client.SetMyCommands(&tbot.RequestSetMyCommands{
		Scope: &tbot.BotCommandScope{
			ChatID: ctx.ChatID,
			Type:   tbot.BotCommandScopeTypeChat,
		},
		Commands: commands,
	})
}
```

## tbot.Runtime example script file

```yaml
# test calculator example

stories:
  default: # started on fail
    - listen
  start: # main entrypoint
    - send_message:
        text: $welcome_text
    - typing_status:
        timeout: 1s
    - select_language
  select_language:
    - select_user_language:
        text: $select_language_text
        ru_text: 🇷🇺 RU
        en_text: 🇺🇸 EN
    - print_instruction
  print_instruction:
    - send_message:
        text: $instruction_text
    - typing_status:
        timeout: 1s
    - listen
  listen:
    - set_commands_and_listen:
        on_start: start
        on_language: select_language
        on_instruction: print_instruction
        on_input_numbers: input_numbers
        msg_text: $listen_msg_text
        cmd_btn_text: $commands_btn_text
        cmd_start: /start
        cmd_start_desc: $commands_desc_start
        cmd_instruction: /instruction
        cmd_instruction_desc: $commands_desc_instruction
        cmd_language: /language
        cmd_language_desc: $commands_desc_language
  input_numbers:
    - enter_number:
        store_to: number_first
        text: $input_number_text_1
        err_text: $input_number_err_text
    - enter_number:
        store_to: number_second
        text: $input_number_text_2
        err_text: $input_number_err_text
    - print_result:
        result_text: $result_text
        where_1: number_first
        where_2: number_second
    - typing_status:
        timeout: 2s
    - listen

strings:
  welcome_text:
    en: |
      Hello!
      I'm a Calculator bot. 🤖
    ru: |
      Здравствуйте!
      Я бот Калькулятор. 🤖
  select_language_text:
    en: Select language
    ru: Выберите язык
  instruction_text:
    en: You type two numbers. We are will calculate result
    ru: Вы просто вводите два числа, а мы вычисляем результат
  commands_btn_text:
    en: Menu
    ru: Меню
  commands_desc_start:
    en: Restart
    ru: Начать с начала
  commands_desc_instruction:
    en: Instruction
    ru: Инструкция
  commands_desc_language:
    en: Change language
  input_number_err_text:
    en: Wrong number
    ru: Неверное число
  input_number_text_1:
    en: Enter first number
    ru: Введите первое число
  input_number_text_2:
    en: Enter second number
    ru: Введите второе число
  result_text:
    en: 'Result'
    ru: 'Результат'
  listen_msg_text:
    en: Select action
    ru: Выберите действие
```
