package tbot

type PollerStorage interface {
	GetLastUpdate() (int64, error)
	PutLastUpdate(id int64) error
}

func newPollerEmptyStorage() PollerStorage {
	return new(pollerEmptyStorage)
}

type pollerEmptyStorage struct{}

func (s *pollerEmptyStorage) GetLastUpdate() (int64, error) { return 0, nil }

func (s *pollerEmptyStorage) PutLastUpdate(id int64) error { return nil }
