package tbot

import (
	"context"
	"sync"

	"gitlab.com/tehidev/go/tbot/codes"
)

type Action func(ctx *Context) error

type ActionParams map[string]string

type Runtime struct {
	desc *Description

	context context.Context
	storage RuntimeStorage

	defaultLanguage codes.Language

	printError func(error)

	actions map[string]Action

	wg sync.WaitGroup
	mx *valueMutex
}

func (r *Runtime) Wait() {
	r.wg.Wait()
}

func (r *Runtime) OnTBotError(err error) {
	r.printError(err)
}

func (r *Runtime) Call(chatID int64, branchName string) error {
	r.wg.Add(1)
	defer r.wg.Done()

	defer r.mx.Lock(chatID)()

	cursor, err := r.storage.GetCursor(chatID)
	if err != nil {
		return &ErrRuntime{
			Err:    err,
			Cursor: &Cursor{ChatID: chatID, BranchName: branchName},
		}
	}
	if cursor == nil {
		cursor = &Cursor{ChatID: chatID}
	}

	cursor.BranchName = branchName
	cursor.BranchLine = 0
	cursor.ActionName = ""

	if err = r.storage.PutCursor(cursor); err != nil {
		return &ErrRuntime{Err: err, Cursor: cursor}
	}

	return r.serveCursorWithError(cursor, nil)
}

func (r *Runtime) ServeUpdate(update *Update) error {
	if update.User() == nil {
		return nil
	}

	r.wg.Add(1)
	defer r.wg.Done()

	chat := update.Chat()
	defer r.mx.Lock(chat.ID)()

	if update.MyChatMember != nil {
		if update.MyChatMember.NewChatMember != nil {
			switch update.MyChatMember.NewChatMember.Status {
			case ChatMemberLeft, ChatMemberBanned:
				return r.storage.DelCursor(chat.ID)
			}
		}
		return nil
	}

	cursor, err := r.storage.GetCursor(chat.ID)
	if err != nil {
		return &ErrRuntime{
			Err:    err,
			Update: update,
			Cursor: &Cursor{ChatID: chat.ID},
		}
	}
	if cursor == nil {
		if update.Message != nil && update.Message.LeftChatMember != nil {
			return nil
		}
		cursor = &Cursor{
			ChatID:     chat.ID,
			Language:   update.User().LanguageCode,
			BranchName: StartBranch,
			BranchLine: 0,
			ActionName: "",
		}
	}
	return r.serveCursorWithError(cursor, update)
}

func (r *Runtime) serveCursorWithError(cursor *Cursor, update *Update) error {
	if err := r.serveCursor(cursor, update); err != nil {
		return &ErrRuntime{
			Err:    err,
			Cursor: cursor,
			Update: update,
		}
	}
	return nil
}

func (r *Runtime) serveCursor(cursor *Cursor, update *Update) error {
	var line *BranchLine

	lines, valid := r.desc.Branches[cursor.BranchName]
	if valid {
		valid = cursor.BranchLine < len(lines)
		if valid {
			line = lines[cursor.BranchLine]
			if cursor.ActionName != "" {
				valid = line.ActionName == cursor.ActionName
			}
		}
	}
	if !valid {
		return r.moveToDefaultBranch(cursor, update)
	}

	if line.IsBranch {
		return r.moveToBranch(line.BranchName, cursor, update)
	}

	if !r.desc.HasLanguage(cursor.Language) {
		cursor.Language = r.defaultLanguage
	}
	cursor.ActionName = line.ActionName

	if err := r.storage.PutCursor(cursor); err != nil {
		return err
	}

	action := r.actions[line.ActionName]

	ctx := &Context{
		Context: r.context,
		Cursor:  cursor,
		Update:  update,
		Params:  line.ActionParams[cursor.Language],
		runtime: r,
	}

	if err := action(ctx); err != nil {
		return err
	}

	if ctx.nextLine {
		cursor.BranchLine++
		if cursor.BranchLine < len(lines) {
			line = lines[cursor.BranchLine]

			cursor.ActionName = line.ActionName

			return r.serveCursor(cursor, nil)
		}
		return r.moveToDefaultBranch(cursor, nil)
	}

	if ctx.nextBranch != "" {
		return r.moveToBranch(ctx.nextBranch, cursor, nil)
	}

	return nil
}

func (r *Runtime) moveToDefaultBranch(cursor *Cursor, update *Update) error {
	return r.moveToBranch(DefaultBranch, cursor, update)
}

func (r *Runtime) moveToBranch(branchName string, cursor *Cursor, update *Update) error {
	cursor.BranchName = branchName
	cursor.BranchLine = 0
	cursor.ActionName = ""

	if update == nil {
		if err := r.storage.PutCursor(cursor); err != nil {
			return err
		}
	}

	return r.serveCursor(cursor, update)
}
