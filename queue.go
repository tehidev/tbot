package tbot

import (
	"io"
	"sync"
)

type Queue struct {
	storage QueueStorage
	handler BotHandler

	chatsWorkers map[int64]struct{}

	done chan struct{}

	mx sync.Mutex
	wg sync.WaitGroup
}

func (q *Queue) OnTBotError(err error) {
	q.handler.OnTBotError(err)
}

func (q *Queue) WorkersCount() int {
	q.mx.Lock()
	defer q.mx.Unlock()
	return len(q.chatsWorkers)
}

func (q *Queue) Stop() {
	close(q.done)
	q.wg.Wait()
}

func (q *Queue) ServeUpdate(update *Update) error {
	select {
	case <-q.done:
		return io.ErrClosedPipe
	default:
		chat := update.Chat()
		if err := q.storage.PushUpdate(chat.ID, update); err != nil {
			return err
		}
		q.checkUpdate(chat.ID)
		return nil
	}
}

func (q *Queue) fetchUpdates() error {
	chats, err := q.storage.GetChats()
	if err != nil {
		return err
	}
	for _, id := range chats {
		q.checkUpdate(id)
	}
	return nil
}

func (q *Queue) checkUpdate(chatID int64) {
	q.mx.Lock()
	defer q.mx.Unlock()
	if _, ok := q.chatsWorkers[chatID]; !ok {
		q.wg.Add(1)
		q.chatsWorkers[chatID] = struct{}{}
		go q.receiveUpdates(chatID)
	}
}

func (q *Queue) receiveUpdates(chatID int64) {
	defer q.wg.Done()
	defer func() {
		q.mx.Lock()
		delete(q.chatsWorkers, chatID)
		q.mx.Unlock()
	}()

	for {
		select {
		case <-q.done:
			return
		default:
			if err := q.receiveUpdate(chatID); err != nil {
				if err != errQueueNoUpdates {
					q.OnTBotError(err)
				}
				return
			}
		}
	}
}

func (q *Queue) receiveUpdate(chatID int64) error {
	update, err := q.storage.PeekUpdate(chatID)
	if err != nil {
		return err
	}
	if update == nil {
		return errQueueNoUpdates
	}

	if err = q.handler.ServeUpdate(update); err != nil {
		return err
	}

	if err = q.storage.PopUpdate(chatID); err != nil {
		return err
	}

	return nil
}
