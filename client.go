package tbot

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
)

type Client struct {
	ctx         context.Context
	ctxCancel   context.CancelFunc
	http        *http.Client
	endPoint    string
	accessToken string
}

type fileRequest interface {
	IsMultipart() bool
	MultipartForm() (data []byte, ctype string, err error)
}

func (c *Client) Stop() {
	c.ctxCancel()
}

func (c *Client) Do(method string, src, dst interface{}) (err error) {
	url := fmt.Sprintf("%s/bot%s/%s", c.endPoint, c.accessToken, method)

	ctype := "application/json"
	var body []byte

	if req, ok := src.(fileRequest); ok && req.IsMultipart() {
		body, ctype, err = req.MultipartForm()
		if err != nil {
			return &ErrClient{Err: err, Method: method, Details: "fileRequest.MultipartForm"}
		}
	} else if src != nil {
		body, err = json.Marshal(src)
		if err != nil {
			return &ErrClient{Err: err, Method: method, Details: "json.Marshal(src)"}
		}
	}

	req, err := http.NewRequestWithContext(c.ctx, http.MethodPost, url, bytes.NewReader(body))
	if err != nil {
		return &ErrClient{Err: err, Method: method, Details: "http.NewRequestWithContext"}
	}
	req.Header.Set("Content-Type", ctype)

	res, err := c.http.Do(req)
	if err != nil {
		return &ErrClient{Err: err, Method: method, Details: "http.Do"}
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return &ErrClient{Code: res.StatusCode, Method: method, Details: http.StatusText(res.StatusCode)}
	}

	data, err := io.ReadAll(res.Body)
	if err != nil {
		return &ErrClient{Err: err, Code: res.StatusCode, Method: method, Details: "io.ReadAll(response.Body)"}
	}
	var result clientResponse
	if err = json.Unmarshal(data, &result); err != nil {
		return &ErrClient{Err: err, Code: res.StatusCode, Method: method, Details: "json.Unmarshal(response)"}
	}
	if !result.OK {
		return &ErrClient{
			Code:    result.ErrorCode,
			Method:  method,
			Details: result.Description,
		}
	}

	if dst == nil {
		return nil
	}
	switch v := dst.(type) {
	case *[]byte:
		*v = result.Result
	case *string:
		*v = string(result.Result)
	case *int:
		i, err := strconv.Atoi(string(result.Result))
		if err != nil {
			return &ErrClient{Err: err, Code: res.StatusCode, Method: method, Details: "strconv.Atoi(response.Result)"}
		}
		*v = i
	case *int64:
		i, err := strconv.ParseInt(string(result.Result), 10, 64)
		if err != nil {
			return &ErrClient{Err: err, Code: res.StatusCode, Method: method, Details: "strconv.ParseInt(response.Result)"}
		}
		*v = i
	default:
		if err = json.Unmarshal(result.Result, dst); err != nil {
			return &ErrClient{Err: err, Code: res.StatusCode, Method: method, Details: "json.Unmarshal(response.Result)"}
		}
	}
	return nil
}

// ==============================================================================

type clientResponse struct {
	OK bool `json:"ok"`

	ErrorCode   int    `json:"error_code"`
	Description string `json:"description"`

	Result clientResult `json:"result"`
}

func (res *clientResponse) Error() string {
	if res.OK {
		return ""
	}
	return fmt.Sprintf("%d: %s", res.ErrorCode, res.Description)
}

type clientResult []byte

func (r *clientResult) UnmarshalJSON(b []byte) error {
	*r = b
	return nil
}

func (r clientResult) MarshalJSON() ([]byte, error) {
	return r, nil
}
