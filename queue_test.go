package tbot_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tehidev/go/tbot"
)

func TestQueue(t *testing.T) {
	accessToken := os.Getenv("QUEUE_ACCESS_TOKEN")
	if accessToken == "" {
		t.SkipNow()
	}

	client, err := tbot.NewClient().
		WithAccessToken(accessToken).
		Build()
	assert.NoError(t, err)

	handler := new(testQueueHandler)

	queue, err := tbot.NewQueue().
		WithHandler(handler).
		Start()
	assert.NoError(t, err)
	defer queue.Stop()

	poller, err := tbot.NewPoller().
		WithClient(client).
		WithHandler(queue).
		Start()
	assert.NoError(t, err)
	defer poller.Stop()

	go func() {
		for {
			println("queue.WorkersCount", queue.WorkersCount())
			time.Sleep(time.Second)
		}
	}()

	time.Sleep(time.Minute * 10)
}

type testQueueHandler struct{}

func (q *testQueueHandler) ServeUpdate(u *tbot.Update) error {
	fmt.Printf("queue.ServeUpdate user(%d) text(%s)\n", u.User().ID, u.Message.Text)
	time.Sleep(time.Second * 2)
	return nil
}

func (q *testQueueHandler) OnTBotError(err error) {
	fmt.Println("queue.OnTBotError", err)
}
