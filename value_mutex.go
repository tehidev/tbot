package tbot

import "sync"

func newValueMutex() *valueMutex {
	return &valueMutex{
		x: make(map[int64]*sync.Mutex),
	}
}

type valueMutex struct {
	x  map[int64]*sync.Mutex
	mx sync.Mutex
}

func (vm *valueMutex) Lock(value int64) (unlock func()) {
	vm.mx.Lock()

	currMutex, ok := vm.x[value]
	if !ok {
		currMutex = new(sync.Mutex)
		vm.x[value] = currMutex
	}

	vm.mx.Unlock()

	currMutex.Lock()

	unlock = func() {
		currMutex.Unlock()

		vm.mx.Lock()
		delete(vm.x, value)
		vm.mx.Unlock()
	}

	return
}
